#include <stdio.h>

void contador(int *a, int s) {

    int i;

    for (i = 0; i < s; i++) {
        printf("Elemento %d = %d\n", i, *a);
        a++;
    }
}

int main() {

    int arr[] = {1, 2, 3, 4, 5};

    contador(arr, 5);

    return 0;
}
