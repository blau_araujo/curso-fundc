#include <stdio.h>

void contador(int *a) {
    printf("%d ", *a);
}

int main() {

    int i, arr[] = {1, 2, 3, 4, 5};

    for (i = 0; i < 5; i++) {
        contador(&arr[i]);
    }

    puts("");

    return 0;
}
