#include <stdio.h>

void troca_letra(char a, char b) {
    char c = a;
    a = b;
    b = c;
}

int main() {

    char x = 'm';
    char y = 'n';

    printf("Antes da troca: %c, %c\n", x, y);

    troca_letra(x, y);

    printf("Depois da troca: %c, %c\n", x, y);

    return 0;
}

