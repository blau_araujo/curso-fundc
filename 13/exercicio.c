#include <stdlib.h>
#include <stdio.h>
#include <time.h>

/*
Precisamos de 'stdlib.h' por causa
das funções 'srand()' e 'rand()'.

Precisamos de 'time.h' por causa
da função 'time()'.
*/

int secreto() {
    unsigned int seed = time(0);

    srand(seed);

    return (rand() % 100) + 1;
}

int palpite() {

    char p[10];

    printf("\nDigite um número entre 1 e 100: ");

    fgets(p, 10, stdin);

    return atoi(p);
}

int testa_palpite(int s) {

        int p = palpite();

        if ((p < 1) || (p > 100)) {
            puts("Eu disse entre 1 e 100!");
        } else if (p > s) {
            puts("Seu palpite foi alto demais!");
        } else if (p < s) {
            puts("Seu palpite foi baixo demais!");
        } else {
            puts("Na mosca!\n");
        }

        return p;
}

int main() {

    int s = secreto();
    int chute;

    while (s != chute) {

        chute = testa_palpite(s);

    }

    return 0;
}

