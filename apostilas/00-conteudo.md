# Fundamentos da Linguagem C - Conteúdo programático

## [1 - Conceitos básicos](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md)

1. [Introdução](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md#11-introdu%C3%A7%C3%A3o)
2. [Linguagens interpretadas e compiladas](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md#12-linguagens-interpretadas-e-compiladas)
3. [O compilador GNU/GCC](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md#13-o-compilador-gnugcc)
4. [A estrutura básica de um programa](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md#14-a-estrutura-b%C3%A1sica-de-um-programa)
5. [As bibliotecas 'stdlib.h' e 'stdio.h'](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md#15-bibliotecas-stdlib-e-stdio)
6. [A função 'main()'](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md#16-a-fun%C3%A7%C3%A3o-main)
7. [Retornos](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md#17-retornos)
8. [A função 'printf()'](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md#18-a-fun%C3%A7%C3%A3o-printf)
9. [Acentuação e a biblioteca 'locale.h'](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md#19-acentua%C3%A7%C3%A3o-e-a-biblioteca-locale)
10. [Olá, mundo!](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/01-conceitos-basicos.md#110-ol%C3%A1-mundo)


## [2 - Tipos de dados](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/02-tipos-de-dados.md)

1. [Tipos primitivos](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/02-tipos-de-dados.md#21-tipos-primitivos)
2. [O tipo 'void'](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/02-tipos-de-dados.md#22-o-tipo-void)
3. [O tipo 'char'](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/02-tipos-de-dados.md#23-o-tipo-char)
4. [O tipo 'int'](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/02-tipos-de-dados.md#24-o-tipo-int)
5. [Os tipos 'float' e 'double'](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/02-tipos-de-dados.md#25-os-tipos-float-e-double)
6. [Tipos modificados](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/02-tipos-de-dados.md#26-tipos-modificados)
7. [O tipo 'bool'](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/02-tipos-de-dados.md#27-tipo-bool)
8. [Strings](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/02-tipos-de-dados.md#28-strings)
9. [Arrays](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/02-tipos-de-dados.md#29-arrays)

## [3 - Constantes](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/03-constantes.md)

1. [Constantes numéricas](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/03-constantes.md#31-constantes-num%C3%A9ricas)
2. [Constantes caracteres](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/03-constantes.md#32-constantes-caracteres)
3. [Constantes strings](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/03-constantes.md#33-constantes-strings)
4. [Constantes simbólicas](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/03-constantes.md#34-constantes-simb%C3%B3licas)

## [4 - Variáveis](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/04-variaveis.md)

1. [Atributos das variáveis](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/04-variaveis.md#41-atributos-das-vari%C3%A1veis)
2. [Nomeando variáveis](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/04-variaveis.md#42-nomeando-vari%C3%A1veis)
3. [Palavras reservadas](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/04-variaveis.md#43-palavras-reservadas)
4. [Declarando e inicializando variáveis](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/04-variaveis.md#44-declarado-e-inicializando-vari%C3%A1veis)
5. [Escopo de variáveis](https://gitlab.com/blau_araujo/curso-fundc/-/blob/master/apostilas/04-variaveis.md#45-escopo-de-vari%C3%A1veis)

## 5 - Ponteiros

1. Valores e referências
2. Indireção
3. Declarando ponteiros
4. Acessando dados através de ponteiros
5. Indireção múltipla
6. Declarando ponteiros para ponteiros

## 6 - Formatação de entrada e saída

1. Cadeias e especificadores de formato
2. Caracteres de controle ANSI-C
3. A função 'scanf' e o operador '&'
4. Formatando a exibição de dados
5. Tamanhos de campos
6. Precisão de dados
7. Lendo conjuntos de caracteres
8. Concatenando dados

## 7 - Argumentos da linha de comando

1. Passagem por valor e por referência
2. A biblioteca 'stdio'
3. Coletando dados da linha de comando
4. Contando os argumentos

## 8 - Operadores e expressões

1. Operadores aritméticos
2. Expressões aritméticas
3. A biblioteca 'math'
4. Operadores de atribuição
5. Expressões compactas
6. Operadores de comparação
7. Expressões lógicas

## 9 - Estruturas de decisão

1. Desvio condicional simples - 'if'
2. Desvio condicional composto - 'if/else'
3. Operador condicional - '?/:' 
4. Desvio condicional aninhado - 'if/else if'
5. Estrutura de decisão múltipla - 'switch'

## 10 - Estruturas de repetição

1. Repetição por contagem - loop 'for'
2. Repetição por precondição - loop 'while'
3. Loop infinito
4. Repetição por poscondição - loop 'do/while'
5. Interrompendo repetições - 'break'

## 11 - Funções

1. Preprocessamento
2. Diretiva '#define'
3. Diretiva '#include'
4. Definindo e usando funções
5. Funções sem retorno
6. Funções com retorno
7. Escopo de duração de variáveis
8. Recursividade
9. Funções recursivas

## 12 - Estruturas

1. Relacionando variáveis logicamente
2. Estrutura de tipo anônimo
3. Estruturas rotuladas
4. Inicialização e aninhamento de estruturas
5. Vetores de estruturas (tabelas)
6. Busca e ordenação em tabelas
7. Estruturas mutantes

## 13 - Trabalhando com arquivos

1. Ponteiros de arquivos
2. Descritores de arquivos
3. Redirecionamentos de entrada e saída
4. Abrindo arquivos
5. Fechando arquivos
6. Checando o fim do arquivo
7. Leitura e escrita de arquivos
8. Leitura e ecrita de caracteres
9. Leitura e escrita binárias

## 14 - Arquivos de cabeçalho

1. Organizando e reaproveitando o código
2. Criando arquivos de cabeçalho
3. A estrutura de um arquivo de cabeçalho
4. A diretiva '#ifndef/#endif'
5. Criando as funções do cabeçalho

