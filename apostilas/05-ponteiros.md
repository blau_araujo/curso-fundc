# 5 - Ponteiros

Um ponteiro é uma variável que contém o endereço de uma outra variável. Isso é muito interessante, porque, utilizando ponteiros, nós podemos apontar para o endereço da variável em vez de apontarmos para o seu valor atual e, caso esse valor mude, não haverá problemas, pois os ponteiros sempre irão indicar onde está o dado atualizado.

# 5.1 - Declarando e acessando ponteiros

A declaração de um ponteiro é feita exatamente como a declaração de uma veriável. A diferença é que nós utilizamos o operador unário de indireção `*`:

```
TIPO *NOME_DO_PONTEIRO;
```

Como os ponteiros armazenam endereços de variáveis, nós precisaremos do operador unário `&` para fazermos as atribuições:

**Exemplo 27**

```
#include <stdio.h>

    int a = 10;
    int *p = &a;

    printf("Valor de a: %d\n"
           "Endereço de a: %p\n"
           "Conteúdo de p: %p\n"
           "Valor apontado por p: %d\n"
           , a, &a, p, *p);

    return 0;
}
```

Na saída...

```
Valor de a: 10
Endereço de a: 0x7fff2cc130c4
Conteúdo de p: 0x7fff2cc130c4
Valor apontado por p: 10
```

> **Nota:** O especificador de formato `%p` converte o valor de um ponteiro numa sequência de caracteres e o `rpintf()` faz a exibição em hexadecimal.

Ainda no exemplo, observe que `&a` faz referência ao endereço da variável em vez do seu valor. Quando a variável `p` foi acessada sem operadores, o valor exibido foi exatamente o endereço de `a`. Já com o operador de indireção (`*`), o valor exibido foi o valor de `a`.

Vamos tentar entender um pouco melhor essa dinâmica com outro exemplo.

**Exemplo 28**

```
#include <stdio.h>

int main() {

    int a = 10, b = 20;
    int *p = &a;

    puts("p = &a");
    printf("*p=%d, a=%d, b=%d\n", *p, a, b);

    b = *p;

    puts("b = *p");
    printf("*p=%d, a=%d, b=%d\n", *p, a, b);

    *p = 0;

    puts("p = 0");
    printf("*p=%d, a=%d, b=%d\n", *p, a, b);

    return 0;
}
```

Quando executado, este exemplo produz a seguinte saída:

```
p = &a
*p=10, a=10, b=20
b = *p
*p=10, a=10, b=10
*p = 0
*p=0, a=0, b=10
```

Aqui nós estamos acompanhando os valores em `a`, `b` e `*p`. Repare que incialmente, com a atribuição `p = &a`, o valor apontado por `p` é o valor em `a`.
```
p = &a
*p=10, a=10, b=20
```

No segundo momento, nós atribuímos o valor apontado por `p` à variável `b`, que passou a ser `10`.

```
b = *p
*p=10, a=10, b=10
```

Finalmente, na terceira atribuição, nós fizemos `*p = 0`, ou seja, nós atribuímos ao endereço apontado pelo ponteiro `p` (endereço de `a`, no caso) o valor `0`, e isso fez com que o valor em `a` passasse a ser `0`. 

```
*p = 0
*p=0, a=0, b=10
```

Sendo assim, nós podemos dizer que uma referência a `*p` é uma referência a `a`.


