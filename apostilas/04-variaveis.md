# 4 - Variáveis

Variáveis são nomes que expressam e apontam para algum valor que pode variar ao longo do programa. Na linguagem C, todas as veriáveis precisam ser declaradas e o seu tipo deve ser especificado para que um espaço na memória seja reservado para a escrita dos valores. Por este motivo, mesmo que o valor varie, o tipo de uma variável deve ser o mesmo durante toda a execução do programa.

## 4.1 - Atributos das variáveis

Toda variável em C possui cinco propriedades fundamentais:

* Seu nome;
* O valor que ela expressa;
* Um endereço na memória;
* Um tamanho em bytes;
* Um tipo.

Quando declaramos uma variável, nós estamos criando uma espécie de compartimento vazio com tamanho suficiente para alocar um valor do tipo especificado. Esses compartimentos serão mapeados em endereços na memória e, posteriormente, o nosso programa utilizará esses endereços para processar os dados.

No fim das contas, o nome de uma variável é apenas um identificador humanamente legível desses endereços e dos valores neles armazenados.

> **Nota:** É importante destacar que o endereço de uma variável indica a primeira posição na memória para um certo dado, o que veremos mais adiante no tópico sobre **ponteiros**.

## 4.2 - Nomeando variáveis

Os nomes de variáveis devem ser escolhidos em conformidade com algumas regras:

* Só podem conter caracteres maiúsculos e minúsculos de A a Z, números e o caractere sublinhado (`_`).
* O primeiro caractere **não pode** ser numérico.
* Não podem conter espaços.
* Não podem coincidir com palavras reservadas da linguagem.

> **Nota:** Embora não seja uma boa prática, a regra sobre o uso de palavras reservadas pode ser "driblada" com o uso de caracteres maiúsculos.

Não é uma regra, mas é boa prática preferir nomes de variáveis contendo caracteres minúsculos, deixando nomes totalmente em maiúsculas para as constantes.

## 4.3 - Palavras reservadas

Aqui estão algumas das palavras reservadas da linguagem C que devem ser evitadas como nomes de identificadores em geral (inclusive nomes de variáveis).

**ANSI-C e ISO C90**

```
auto     double int      struct
break    else   long     switch
case     enum   register typedef
char     extern return   union
const    float  short    unsigned
continue for    signed   void
default  goto   sizeof   volatile
do       if     static   while
```

**ISO C99**

```
inline
restrict
_Bool
_Complex
_Imaginary
```

**ISO C11**

```
_Alignas
_Alignof
_Atomic
_Generic
_Noreturn
_Static_assert
_Thread_local 
```

Além dessas, toda palavra iniciada com dois sublinhados (`__`) ou um sublinhado seguido de maiúscula é reservada.

## 4.4 - Declarado e inicializando variáveis

A declaração de variáveis envolve a especificação do tipo e do nome da variável:

```
TIPO NOME;
```

Também é possível declarar mais de uma variável numa mesma instrução, desde que elas sejam do mesmo tipo:

```
TIPO NOME1, NOME2, NOME3... ;
```

Opcionalmente, as variáveis podem ser inicializadas com a atribuição de um valor utilizando o operador de atribuição `=` seguido de uma constante (literal) ou de uma expressão:

```
TIPO NOME = VALOR;
```

Sem a inicialização, a variável declarada terá um valor indefinido inicial chamado de *garbage* ou "lixo", que é basicamente algo anteriormente registrado naquele endereço. É preciso ter algum cuidado com isso, pois o uso de uma variável declarada sem que haja uma atribuição de valor posteriormente pode levar a resultados imprevisíveis.

Por exemplo:

```
int a;

a = a + 1
    ^
    |
    +-- não inicializada, valor indefinido!
```

Neste caso, se compilarmos com a opção `-Wall`, nós até seremos avisados sobre a variável estar sendo utilizada sem uma inicialização, mas o programa será compilado de qualquer forma. 

Ainda sobre isso, também devemos ter cuidado com as declarações múltiplas desse tipo:

```
int a, b, c = 10;
```

Aqui, apenas a variável `c` foi inicializada, mas os valores de `a` e `b` ainda estão indefinidos. Observe o exemplo abaixo (exemplo25.c):

```
#include <stdio.h>

int main() {
    int a, b, c = 10;

    printf("a %d, b %d, c %d\n", a, b, c);

    return 0;
}
```

Mesmo com o aviso, o código compilou resultando na seguinte saída:

```
:~$ ./exemplo25 
a 0, b 32765, c 10
```

Aqui está outro exemplo de inicialização a que devemos estar atentos:

**Exemplo 26**

```
#include <stdio.h>

int main() {
    int a = b = c = 10;

    printf("a %d, b %d, c %d\n", a, b, c);

    return 0;
}
```

Aqui, o operador `=` tentaria efetuar a atribuição em pares de operandos da direita para a esquerda, começando com a expressão `c = 10`, onde `c` é uma variável que ainda não foi declarada. Consequentemente, nós teríamos um erro de compilação. Ou seja, nas atribuições múltiplas, todas as variáveis envolvidas precisam estar previamente declaradas. Isso, por exemplo, funcionaria:

```
#include <stdio.h>

int main() {
    int b;
    int c = 10;
    int a = b = c;

    printf("a %d, b %d, c %d\n", a, b, c);

    return 0;
}
```

## 4.5 - Escopo de variáveis

Na linguagem C, as variáveis podem ser declaradas em três pontos diferentes no código:

* Dentro de funções;
* Fora de funções;
* Nos parâmetros das funções.

Cada um desses pontos determinará as regiões do código que terão acesso a elas. O escopo é justamente isso: a região do programa em que é possível acessar uma variável. 

### 4.5.1 - Variáveis locais

Quando as variáveis são declaradas dentro de uma função, elas não podem ser acessadas fora da função e nem utilizadas diretamente por outras funções. Além disso, elas só passam a existir quando a função é chamada e são imediatamente destruídas quando a função termina a sua execução.

Também são locais as variáveis declaradas como parâmetros de funções, ou seja, elas só podem ser acessadas diretamente pela função de que ela é um parâmetro e é destruída com o fim da execução da função.

### 4.5.2 - Variáveis globais

Uma variável global é declarada fora de funções e, com isso, ela torna-se disponível em qualquer parte do programa, inclusive dentro de blocos de funções. Como não estão atreladas a funções, as variáveis globais são destruídas apenas quando o programa termina a sua execução.

> **Nota:** Não é uma boa prática (nem uma boa ideia), mas se uma variável for local e outra for global, elas podem ter o mesmo nome. Caso isso ocorra, a variável local sempre terá a prioridade dentro do seu escopo.


