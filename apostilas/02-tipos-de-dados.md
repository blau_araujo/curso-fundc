# 2 - Tipos de dados

A lingugem C oferece 5 tipos **primitivos** de dados, os quais podem ter seus limites e tamanhos na memmória alterados por 4 **modificadores**. A combinação desses tipos e modificadores é que dão forma à semântica e às caracteríticas de armazenamento dos tipos de dados.

Outra classificação dos tipos de dados seria quanto à sua apresentação na forma original ou derivada de várias formas de agrupamentos. Neste caso, os tipos seriam considerados **primários** (forma original) ou **derivados**.

## 2.1 - Tipos primitivos

Os tipos primitivos são os **caracteres**, os **tipos aritméticos** e o tipo especial `void`. Cada um desses tipos possui uma palavra chave, um tamanho em bytes e um intervalo de valores máximos que pode variar de acordo com a plataforma.

## 2.2 - O tipo 'void'

O tipo "vazio" só é considerado um tipo de dado para efeito de coerência semântica e como um componente da sintaxe para dizer ao compilador, através da palavra chave `void`, que a função não retorna valores ou que ela não pode receber parâmetros.

```
// A função não retorna valores...
void funcname() {
    [instruções]
}
```

As funções com retorno do tipo `void` são aquelas que executam alguma sub-rotina e não devolvem um valor para a função chamadora. Portanto, elas não podem participar de expressões. A instrução abaixo, por exemplo, causaria um erro de compilação:

```
a = funcname() + 1;
```

Quando a palavra chave `void` aparece na lista de parâmetros da função, o compilador acusaria um erro se algum parâmetro fosse passado para ela. Por exemplo:

```
// A função não recebe parâmetros...
int funcname(void) {
    [instruções]
    return 0;
}
```

Chamar a função desta forma causaria um erro de compilação:

```
funcname(10);
```

**Nota:** Se funções do tipo vazio não podem participar de expressões, não faz sentido declarar uma variável com a palavra chave `void`. No entanto, existe uma sintaxe que pode causar confusão e nos levar a imaginar que isso é possível, que é quando declaramos um **ponteiro** para dados de tipo indefinido: `void *`. Apenas nesse contexto, `void` atuaria como uma espécie de tipo universal, ou seja, capaz de receber dados de qualquer tipo. Mas isso é assunto para bem mais adiante.

## 2.3 - O tipo 'char'

Para o compilador, tudo são números. Portanto, a linguagem C não diferencia a representação de um caractere de seu valor decimal na tabela ASCII, e é isso que armazenamos numa variável quando atribuímos um caractere a ela -- um número inteiro entre -128 e +127.

Para representar os caracteres, o computador precisa mapear esses valores inteiros e atribuir a cada um deles um código numérico, e um dos códigos mais usados é o ASCII (*American Standard Code for Information Interchange*) -- é a famosa tabela ASCII, que representa 128 caracteres na sua forma padrão, entre letras, números sinais gráficos de pontuação e caracteres não-imprimíveis (caracteres de controle).

Repare que os valores entre `0` e `+127` (128, no total) correspondem ao número de caracteres da tabela ASCII padrão, e é o máximo de valores que podemos exprimir utilizando apenas 7bits (`2**7=128`). Com o bit restante, do total de 8 bits de um byte, é possível acrescentar a representação de mais 127 caracteres de duas formas: considerando o primeiro bit à esquerda como um sinal de negativo (bit de sinal), ou o byte todo como o equivalente binário de um valor maior do que 127.

Originalmente, o primeiro bit a esquerda era utilizado apenas como parte de algum protocolo de comunicação (bit de paridade) e, posteriormente, ele também foi aproveitado (por outros padrões, não pelo padrão ASCII) para ampliar a capacidade de resentação de símbolos gráficos. Portanto, a tabela ASCII em si, continua a mesma, com seus 128 caracteres de sempre. O que varia é o uso que se dá em outros mapas de caracteres para o bit excedente -- e são muitos padrões diferentes.

### 2.3.1 - Uso do tipo caractere

Nós declaramos um valor do tipo caractere utilizando a palavra chave `char` e expressamos seu valor literal constante como um caractere alfnumérico ou um sinal gráfico entre aspas simples.

**Exemplo 1**

```
#include <stdio.h>

int main() {

    char c = 'A';

    printf("Caractere: %c\n", c);

    return 0;
}

```

O especificador de formato associado aos caracteres é o `%c`, como podemos ver no exemplo, e ele faz com que seja impressa a representação gráfica do caractere. Mas, como dissemos, um caractere é, em essência um número, portanto:

**Exemplo 2**

```
#include <stdio.h>

int main() {

    char c = 'A';

    printf("Caractere: %c\n", c);
    printf("Caractere (decimal): %d\n", c);
    printf("Caractere (octal)  : %o\n", c);
    printf("Caractere (hexa)   : %x\n", c);

    return 0;
}

```

Onde:

* `%d` - representa um número em base 10 (decimal)
* `%o` - representa um número em base 8 (octal)
* `%x` - representa um número em base 16 (hexadecimal)

**Saída do Exemplo 2**

```
Caractere          : A
Caractere (decimal): 65
Caractere (octal)  : 101
Caractere (hexa)   : 41
```

Mas também é possível fazer algo assim:

```
#include <stdio.h>

int main() {

    char c = 88;

    printf("Caractere: %c\n", c);
    printf("Caractere (decimal): %d\n", c);
    printf("Caractere (octal)  : %o\n", c);
    printf("Caractere (hexa)   : %x\n", c);

    return 0;
}

```

O que resultaria em:

```
Caractere          : X
Caractere (decimal): 88
Caractere (octal)  : 130
Caractere (hexa)   : 58
```

Afinal, para o compilador, os caracteres são números inteiros.


> **Importante!** Caracteres literais fora da tabela ASCII **padrão** são dependentes da plataforma e geralmente possuem mais de um byte de comprimento. Sendo assim, eles devem ser declarados como cadeias de caracteres (strings), o que veremos mais para frente, mas aqui está um exemplo de como poderia ser feito:

**Exemplo 3**

```
#include <stdio.h>

int main() {

    char c[] = "Á";

    printf("Caractere          : %s\n", c);
    printf("Caractere (decimal): %d\n", c);
    printf("Caractere (octal)  : %o\n", c);
    printf("Caractere (hexa)   : %x\n", c);

    return 0;
}
```

O que produziria a saída...

```
Caractere          : Á
Caractere (decimal): -2090173539
Caractere (octal)  : 20332475635
Caractere (hexa)   : 836a7b9d
```

**Nota:** O exemplo acima compila e executa, mas gera vários avisos.

## 2.4 - O tipo 'int'

Por padrão, um tipo inteiro é qualquer valor numérico inteiro, com sinal, e no intervalo entre `-2**((8*TAMANHO)-1)` a `2**((8*TAMANHO)-1) - 1`. Como o tamanho em bytes do tipo inteiro pode variar com a arquitetura do computador (32 ou 64 bits), os limites podem ser encontrados através das constantes `INT_MAX` e `INT_MIN`, ambas definidas no cabeçalho `limits`, enquanto o tamanho em bytes do tipo inteiro pode ser obtido com o operador `sizeof()`:

**Exemplo 4**

```
#include <limits.h>
#include <stdio.h>

int main() {

    printf("Tamanho em bytes: %d\n", sizeof(int));
    printf("Inteiro máximo: %d\n", INT_MAX);
    printf("Inteiro mínimo: %d\n", INT_MIN);

    return 0;
}
```

O que retornaria em uma aruitetura 64 bits:

```
Tamanho em bytes: 4
Inteiro máximo: 2147483647
Inteiro mínimo: -2147483648
```

### 2.4.1 - Constantes inteiras e constantes caractere

De modo geral, os valores constantes literais dos inteiros são declarados como números inteiros válidos, sem aspas e com a palavra chave `int`:

```
int a = 789;
```

Mas, como vimos, os caracteres também estão nos limites dos inteiros, só que ocupam apenas um byte. Portanto, podemos dizer que as constantes caractere são valores do tipo inteiro, só que, ao serem atribuídos a uma variável do tipo `char`, eles serão inteiros que ocupam apenas um byte na memória.

**Exemplo 5**

```
#include <stdio.h>

int main() {

    char c;

    for (c = 'A'; c <= 'Z'; ++c) {
        printf("Caractere %c - Inteiro %d\n", c, c);
    }

    return 0;
}
```

## 2.5 - Os tipos 'float' e 'double'

Os tipos `float` e `double` são utilizados para armazenar valores numéricos do conjunto dos **números reais**, englobando os número interios e os números fracionários (racionais e irracionais) -- em outras palavras: nós podemos utilizar números com o ponto de separação de casas decimais. 

Quando participa de operações aritméticas, a representação literal de valores do tipo `float` e `double` deve conter o ponto decimal. Caso contrário, eles erão tratados como inteiros:

**Exemplo 6.1**

```
#include <stdio.h>

int main() {

    float divide;

    divide = 5 / 3;

    printf("Divisão: %f\n", divide);

    return 0;
}
```

O que produz na saída...

```
Divisão: 1.000000
```

Contudo, se um dos termos contiver um valor com casas decimais...

**Exemplo 6.2**

```
#include <stdio.h>

int main() {

    float divide;

    divide = 5 / 3.0;

    printf("Divisão: %f\n", divide);

    return 0;
}
```

Resultando em...

```
Divisão: 1.666667
```

O mesmo não ocorre quando ao menos um dos termos da operação for uma variável declarada como `float` ou `double`:


**Exemplo 6.3**

```
#include <stdio.h>

int main() {

    float divide;
    float divisor = 3;

    divide = 5 / divisor;

    printf("Divisão: %f\n", divide);

    return 0;
}
```

O que exibe na saída...

```
Divisão: 1.666667
```

### 2.5.1 - Limites e diferenças dos tipos 'float' e 'double'

A diferença entre os tipos `float` e `double`, é que o `double` ocupa o dobro de espaço na memória, o que permite aumentar a precisão numérica dos valores armazenados. Por este motivo, dizemos que o tipo `float` tem **precisão simples** e o tipo `double` tem **precisão dupla**.

Para conferirmos o espaço ocupado pelos tipos `float` e `double`, nós podemos utilizar o operador `sizeof()`, mas vamos precisar do cabeçalho `float.h` para descobrirmos seus limites e a sua precisão:

**Exemplo 7**

```
#include <stdio.h>
#include <float.h>

int main() {

    printf(
           "\nTIPO FLOAT\n"
           "----------------\n"
           "Tamanho (bytes): %ld \n"
           "Mínimo         : %E \n"
           "Máximo         : %E \n"
           "Precisão       : %d \n"

           "\nTIPO DOUBLE\n"
           "----------------\n"
           "Tamanho (bytes): %ld \n"
           "Mínimo         : %E \n"
           "Máximo         : %E \n"
           "Precisão       : %d \n\n",
           sizeof(float),
           FLT_MIN,
           FLT_MAX,
           FLT_DIG,
           sizeof(double),
           DBL_MIN,
           DBL_MAX,
           DBL_DIG
           );

    return 0;
}
```

Exibindo...

```
TIPO FLOAT
----------------
Tamanho (bytes): 4 
Mínimo         : 1.175494E-38 
Máximo         : 3.402823E+38 
Precisão       : 6 

TIPO DOUBLE
----------------
Tamanho (bytes): 8 
Mínimo         : 2.225074E-308 
Máximo         : 1.797693E+308 
Precisão       : 15 
```

> **Nota:** Os especificadores de formato padrão seriam `%f` para `float` e `%lf` para `double`. Alternativamente, podemos formatar seus valores com a notação exponencial (científica, de engenharia, potência de dez, etc...) `%e` ou `%E`.

Como podemos ver, o limite de precisão do tipo `float` é de 6 casas decimais, enquanto o tipo `double` tem uma precisão de 15 casas decimais -- o que pode variar conforme a arquitetura e o compilador.

O exemplo abaixo nos permite visualizar a diferença de precisão entre os dois tipos. Como o limite do tipo `double` é de 15 casas, vejamos o que acontece quando pedimos para a saída ser formatada com 20 casas decimais.

**Exemplo 8**

```
#include <stdio.h>

int main() {

    float f  = 5.0 / 3.0;
    double d = 5.0 / 3.0;

    printf(
           "Float : 5 / 3 = %.20f\n"
           "                  .....|\n"
           "Double: 5 / 3 = %.20lf\n"
           "                  ..............|\n",
           f,
           d
           );

    return 0;
}
```

A saída do programa mostra o último dígito **sem aproximação** capaz de ser representado corretamente em texto em cada um dos dois tipos:

```
Float : 5 / 3 = 1.66666662693023681641
                  .....|
Double: 5 / 3 = 1.66666666666666674068
                  ..............|
```

## 2.6 - Tipos modificados

Além dos tipos primitivos, a linguagem C oferece quatro modificadores que, combinados com as declarações primitvas, alteram suas faixas de valores possíveis:

| Modificador | Descição                        |
|-------------|---------------------------------|
| `signed`    | Aceita valores com ou sem sinal |
| unsigned    | Só aceita valores sem sinal     |
| short       | Reduz para 2 bytes de espaço    |
| long        | Dobra o espaço em bytes         |

Canonicamente, esses modificadores seriam aplicáveis com as seguintes combinações:

| Modificador     | Tipo   |
|-----------------|--------|
| signed (padrão) | char   |
| unsigned        | char   |
| signed (padrão) | int    |
| unsigned        | int    |
| short           | int    |
| long            | int    |
| long            | double |

O exemplo abaixo mostra as macros (constantes) e os limites máximos e mínimos de cada um desses tipos modificados:

**Exemplo 9**

```
#include <stdio.h>
#include <limits.h>
#include <float.h>

int main() {

    printf(
    "\nunsigned char: %ld byte\n"
    "signed char  : %ld byte\n"
    "SCHAR_MIN    : %d\n"
    "SCHAR_MAX    : %d\n"
    "UCHAR_MAX    : %d\n",
    sizeof(unsigned char),
    sizeof(signed char),
    SCHAR_MIN,
    SCHAR_MAX,
    UCHAR_MAX
    );

    printf(
    "\nunsigned int      : %ld bytes\n"
    "signed int        : %ld bytes\n"
    "short int         : %ld bytes\n"
    "unsigned short int: %ld bytes\n"
    "long int          : %ld bytes\n"
    "unsigned long int : %ld bytes\n"
    "INT_MIN           : %d\n"
    "INT_MAX           : %d\n"
    "UINT_MAX          : %u\n"
    "SHRT_MIN          : %d\n"
    "SHRT_MAX          : %d\n"
    "USHRT_MAX         : %u\n"
    "LONG_MIN          : %ld\n"
    "LONG_MAX          : %ld\n"
    "ULONG_MAX         : %lu\n",
    sizeof(unsigned int),
    sizeof(signed int),
    sizeof(short int),
    sizeof(unsigned short int),
    sizeof(long int),
    sizeof(unsigned long int),
    INT_MIN,
    INT_MAX,
    UINT_MAX,
    SHRT_MIN,
    SHRT_MAX,
    USHRT_MAX,
    LONG_MIN,
    LONG_MAX,
    ULONG_MAX
    );

    printf(
    "\nlong double: %ld bytes\n"
    "LDBL_MIN   : %LE\n"
    "LDBL_MAX   : %LE\n",
    sizeof(long double),
    LDBL_MIN,
    LDBL_MAX
    );

    return 0;
}
```
Executando...

```
unsigned char: 1 byte
signed char  : 1 byte
SCHAR_MIN    : -128
SCHAR_MAX    : 127
UCHAR_MAX    : 255

unsigned int      : 4 bytes
signed int        : 4 bytes
short int         : 2 bytes
unsigned short int: 2 bytes
long int          : 8 bytes
unsigned long int : 8 bytes
INT_MIN           : -2147483648
INT_MAX           : 2147483647
UINT_MAX          : 4294967295
SHRT_MIN          : -32768
SHRT_MAX          : 32767
USHRT_MAX         : 65535
LONG_MIN          : -9223372036854775808
LONG_MAX          : 9223372036854775807
ULONG_MAX         : 18446744073709551615

long double: 16 bytes
LDBL_MIN   : 3.362103E-4932
LDBL_MAX   : 1.189731E+4932
```

> **Importante!** Para ser considerado um valor do tipo `long double`, a representação literal deve possuir um ponto decimal e terminar com o sufixo `L` ou `l`.

O tipo `long double` também oferece um aumento de precisão (precisão estendida), o que pode ser determinado através da constante `LDBL_DIG`, também no cabeçalho `float.h`:

**Exemplo 10**

```
#include <stdio.h>
#include <float.h>

int main() {

    long double ld = 5.0 / 3.0L;

    printf(
           "Long Double: 5 / 3 = %.20LF\n"
           "                       .................|%d\n",
           ld,
           LDBL_DIG
           );

    return 0;
}
```

O que resultaria em...

```
Long Double: 5 / 3 = 1.66666666666666666663
                       .................|18
```

## 2.7 - Tipo 'bool'

Nas primeiras versões da linguagem C, não havia um tipo específico para representar a verdade de uma expressão (`true` ou `false`), o que só foi introduzido a partir do padrão ISO C99 com a palavra chave `_Bool` e introduzindo, ao mesmo tempo, a **macro** `bool` no cabeçalho `stdbool.h`.

> **Nota:** O termo **macro** (de "macroinstrução") na linguagem C refere-se a um fragmento de código que recebe um nome -- quando o nome é usado, ele é substituído pelo conteúdo da macro. Existem vários tipos de macros, e tudo que nós chamamos de "constantes" até aqui são, para sermos mais exatos, macros do tipo "objeto" (ou *object-like*), geralmente utilizadas para declarar constantes com a diretiva `#define`.

De modo geral, o inteiro `0` representa o valor booleano `false` (falso), e o inteiro `1` representa o valor `true` (verdadeiro). Contudo, para utilizarmos a palavra chave `bool` na definição de tipos booleanos, e `true` e `false` como seus valores, nós temos incluir o cabeçalho `stdbool.h` nos nossos códigos.

O que o cabeçalho `stdbool.h` faz é definir:

* `bool` como um alias (apelido) para `_Bool`
* `true` como um alias para `1`
* `false` como um apelido para `0`

Vejamos um exemplo do que aconteceria sem o cabeçalho `stdbool.h`:

**Exemplo 11-a**

```
#include <stdio.h>

int main() {

    _Bool estado = 23;

    printf("O estado booleano é: %d\n", estado);

    estado = 0;

    printf("O estado booleano é: %d\n", estado);

    return 0;
}
```

O que exibiria...

```
O estado booleano é: 1
```

Isso acontece porque qualquer valor diferente de zero é interpretado como `1`, verdadeiro. Portanto, podemos entender que declarar uma expressão como booleana é o mesmo reduzir seu valor a duas possibilidades: `0` ou `1`.

Com o cabeçalho `stdbool.h`, nós podemos utilizar a macro `bool` e as expressões `true` e `false`:

**Exemplo 11-b**

```
#include <stdio.h>
#include <stdbool.h>

int main() {

    bool estado = 234;

    printf("O estado booleano é: %d\n", estado);

    estado = true;

    printf("O estado booleano é: %d\n", estado);

    estado = false;

    printf("O estado booleano é: %d\n", estado);

    return 0;
}

```

O que exibe...

```
O estado booleano é: 1
O estado booleano é: 1
O estado booleano é: 0
```

Em termos de espaço na memória, a teoria diz que `true` e `false` são um bit `0` ou um bit `1`, mas a prática exige que o tamanho mínimo de um valor seja de 1 byte. Portanto, um valor do tipo bool sempre ocupará 1 byte na memória. Em binário:

* `true`  = 00000001
* `false` = 00000000

## 2.8 - Strings

Não existe nativamente um tipo nomeado como "string" na linguagem C. A rigor, no C uma string é uma **array de caracteres** terminada com o caractere nulo `\0`.

Sendo assim, nós podemos declarar strings a partir de um declaração do tipo `char`, o nome da variável seguido de colchetes indicando a quantidade de caracteres que ela irá conter incluindo o caractere nulo na contagem. Por exemplo:

```
char minha_string[10]
```

Essa declaração informa que `minha_string` terá 10 caracteres incluindo o caractere nulo ao final. Então, vejamos o exemplo abaixo:

```
#include <stdio.h>

int main() {

    char fruta[6] = "banana";

    printf("A fruta é %s\n", fruta);

    return 0;
}

```

Aqui, `banana` é uma string com 6 caracteres, o que nos induz a pensar em indicar o número `6` na declaração da variável `fruta`. Mas, veja o que aconteceria na execução do programa:

```
A fruta é bananapa��0V
```

Isso aconteceu porque nós não consideramos o último caractere de toda string: o caractere nulo. Para corrigir nosso exemplo:

```
#include <stdio.h>

int main() {

    char fruta[7] = "banana";

    printf("A fruta é %s\n", fruta);

    return 0;
}
```

Cuja saída seria...

```
A fruta é banana
```

**Nota:** Observe que, apesar de não haver um tipo nativo para strings, existe um especificador de formato para elas: `%s`, com o qual podemos ler e exibir strings diretamente. Sem esse especificador, utilizando o `%c` em seu lugar, nós teríamos que percorrer cada caractere da string para que ela fosse impressa completamente na saída:

```
#include <stdio.h>

int main() {

    char fruta[7] = "banana";
    int i = 0;

    printf("A fruta é ");

    while (fruta[i] != '\0') {
        printf("%c", fruta[i]);
        i++;
    }

    printf("\n");

    return 0;
}
```

Resultando em...

```
A fruta é banana
```

Outro detalhe importante, é que só podemos representar valores literais de strings entre **aspas duplas** ou como uma lista (array) de caracteres:

```
char fruta[7] = "banana";
```

Ou...

```
char fruta[7] = {'b', 'a', 'n', 'a', 'n', 'a', '\0'};
```

### 2.8.1 - O problema da acentuação

Mas, como já vimos, caracteres acentuados e cedilha não fazem parte do conjunto dos 128 caracteres da tabela ASCII e são interpretados conforme a tabela de caracteres utilizada pelo nosso sistema operacional, que gralmente utiliza símbolos que ocupam mais de um byte (*multibyte*). Portanto, veja o que aconteceria com a palavra `maçã` no exemplo abaixo:

```
#include <stdio.h>

int main() {

    char fruta[5] = "maçã";

    printf("A fruta é %s\n", fruta);

    return 0;
}
```

Isso geraria um aviso no compilador:

```
programa.c: In function ‘main’:
programa.c:5:21: warning: initializer-string for array of chars is too long
    5 |     char fruta[5] = "maçã";
      |                     ^~~~~~~~
```

Consequentemente, a saída seria truncada:

```
A fruta é maç�p�v �U
```

Na verdade, nós precisaríamos indicar um tamanho de 7 caracteres para retornar a string `maçã` corretamente, e isso ainda pode variar com a plataforma.

Então, para evitar esse tipo de dificuldade, a linguagem C permite que as strings sejam declaradas sem a definição de uma quantidade de caracteres, desde que seja feita uma inicialização da variável durante a declaração:

```
#include <stdio.h>

int main() {

    char fruta[] = "maçã";

    printf("A fruta é %s\n", fruta);

    return 0;
}
```

O que exibiria...

```
A fruta é maçã
```

### 2.8.2 - O problema da atribuição de valores a strings

Por serem de fato arrays, as strings não podem receber valores com o operador de atribuição `=` em outros pontos do código além da inicialização, como faríamos com os tipos primitivos. O código abaixo, por exemplo, acusaria um erro na compilação:

```
#include <stdio.h>

int main() {

    char fruta[];

    fruta = "maçã";

    printf("A fruta é %s\n", fruta);

    return 0;
}

```

O que resultaria em...

```
programa.c: In function ‘main’:
programa.c:5:10: error: array size missing in ‘fruta’
    5 |     char fruta[];
      |          ^~~~~
programa.c:7:11: error: assignment to expression with array type
    7 |     fruta = "maçã";
      |           ^

```

O motivo está nos colchetes vazios (`[]`), que fazem com que seja disponibilizado apenas um espaço em memória do tamanho necessário para conter os elementos da array que for informada após o operador `=`. Ou seja, de forma alguma esta sintaxe deve ser interpretada como se a array pudesse receber qualquer quantidade de caracteres.

**Nota:** Nós veremos mais sobre isso quando falarmos especificamente de arrays. Por hora, vamos conhecer algumas soluções utilizando o cabeçalho `string.h`.

### 2.8.3 - Função 'strcpy()'

```
strcpy(NOME, "STRING")
```

A função `strcpy()` esvazia a array em NOME e copia para ela os caracteres em STRING. Para que isso ocorra sem erros, a variável NOME deve ter sido declarada com espaço suficiente para conter os caracteres de STRING mais o caractere nulo.

**Exemplo 12**

```
#include <stdio.h>
#include <string.h>

int main() {

    char fruta[15]; 

    strcpy(fruta, "banana");
    printf("A fruta é %s\n", fruta);

    strcpy(fruta, "abacaxi");
    printf("A fruta é %s\n", fruta);

    strcpy(fruta, "maçã");
    printf("A fruta é %s\n", fruta);

    return 0;
}
```

Que resulta em...

```
A fruta é banana
A fruta é abacaxi
A fruta é maçã
```

### 2.8.4 - Funções 'strcat()' e 'strlen()'

```
strcat(NOME, "STRING");
```

Concatena STRING (origem) ao final de NOME (destino). Na concatenação, o caractere nulo da string de destino é trocado pelo primeiro caractere da string de origem e é movido para o final da string resultante.

```
strlen("STRING");
```

Calcula o comprimento de STRING sem o caractere nulo.


A função `strcat()` oferece uma solução simples para a alteração de uma string. Mas existe um problema: assim como em `strcpy()`, o limite de espaço definido na declaração de uma string deveria ser respeitado e reportado como erro na compilação, mas não é.

É o que podemos ver no exemplo abaixo, onde utilizamos a função `strlen()` para determinar a quantidade final de caracteres na string:

**Exemplo 13**

```
#include <stdio.h>
#include <string.h>

int main() {

    char fruta[15];

    strcpy(fruta, "banana");
    printf("A fruta era %s.\n", fruta);

    strcat(fruta, " abacaxi laranja");
    printf("A string '%s' ocupa %ld bytes?!\n",
           fruta,
           sizeof(fruta));

    printf("Agora a string tem %ld caracteres!\n",
	       strlen(fruta));

    return 0;
}
```

O que exibe na saída...

```
A fruta era banana.
A string 'banana abacaxi laranja' ocupa 15 bytes?!
Agora a string tem 22 caracteres.
```

Mesmo eventualmente funcionando, o comportamento desse tipo de código pode levar a resultados indesejados (como um erro de "falha de segmentação") e é considerado inseguro. Então, é muito importante alocar bytes suficientes para conter strings mais longas -- ou ser econômico, mas acompanhar de perto o que acontece a cada concatenação.

> **Nota:** Uma forma mais avançada de trabalho com a função `strcat()` envolveria técnicas de alocação dinâmica de memória para lidar com esse problema.

Uma outra alternativa para o problema, é a função `strncat()`, onde podemos limitar a concatenação a um número máximo de caracteres da string de origem:

** Exemplo 14**

```
#include <stdio.h>
#include <string.h>

int main() {

    char destino[15] = "0123456789";
    char origem[]    = "abcdefghij";

    puts(destino);

    strncat(destino, origem, 14 - strlen(destino));

    puts(destino);

	return 0;
}
```

Aqui, nós limitamos a concatenação à quantidade máxima de caracteres em `destino` (15), menos o seu caractere nulo, e menos o comprimento atual da string em `destino` (10), resultando em 4 caracteres disponíveis para a concatenação. O que resulta em...

```
0123456789
0123456789abcd
```
Mas, por que a sintaxe da função `strcat()` trata seus argumentos como **origem** e **destino**?

A explicação está na descrição que fizemos da função: *o caractere nulo da string de destino é trocado pelo primeiro caractere da string de origem*. Isso quer dizer que, internamente, a função buscará o endereço na memória onde o caractere nulo está armazenado, e este endereço será o "destino" do primeiro caractere da string de origem.

### 2.8.5 - Imprimindo strings com 'puts()' e 'fputs()'

No exemplo anterior, nós utilizamos Uma opção um pouco mais econômica e segura do que `printf()` para imprimir strings na saída padrão: a função `puts()`, do cabeçalho `stdio.h`.

```
puts("STRING");
```

A função `puts()` imprime uma string na saída padrão e inclui uma quebra de linha. Caso a quebra de linha não seja desejada, a alternativa é utilizar a função `fputs()`, também no cabeçalho `stdio.h`, originalmente voltada à escrita de strings em ponteiros de arquivos, mas que pode ser utilizada para escrever diretamente na saída padrão (`stdout`).

**Exemplo 15**

```
#include <stdio.h>

int main() {

    char fruta[15] = "banana";

    // Aqui não temos quebra de linha...
    fputs("A fruta é: ", stdout);

    // Aqui temos quebra de linha...
    puts(fruta);

    return 0;
}
```

A saída exibiria...

```
A fruta é: banana
```

> *Nota:** O cabeçalho `string.h` fornece várias outras funções interessantes para o trabalho com strings, mas o nosso foco no momento é conhecer as peculiaridades dos tipos de dados. Mesmo assim, nós veremos algumas outras funções nos tópicos futuros.

### 2.8.6 - A função sprintf

Da mesma forma que a função `printf()` formata e concatena valores para exibição na saída padrão, a função `sprintf()` formata e concatena valores produzindo uma string. Em termos de sintaxe, `sprintf()` difere de `printf()` apenas pelo fato de receber um parâmetro antes da string de formatação que irá indicar a variável de destino:

```
sprintf(VAR_STRING, "FORMATO", [VALORES]);
``` 

Como a variável de destino é informada como primeiro parâmetro, a função `sprintf()` na linguagem C se distingue das implementações em outras linguagens por retornar o número de caracteres escritos ou, em caso de falha, um número negativo.

**Exemplo 16**

```
#include <stdio.h>

int main() {

    char str[80];
    int retorno;

    retorno = sprintf(str, "Olá, mundo!");

    printf("string    : %s\ncaracteres: %d\n",
           str,
           retorno
           );

    return 0;

}
```

Executando...

```
string    : Olá, mundo!
caracteres: 12
```

## 2.9 - Arrays

Uma array é um tipo de dado composto que constitui uma estrutura capaz de armazenar um conjunto de elementos contendo valores do mesmo tipo. Nela, cada valor ocupa uma posição específica identificada por um índice. Na linguagem C, as arrays possuem tamanho fixo e podem ter várias uma ou mais dimensões.

Para declarar uma array, nós utilizamos a palavra chave correspondente ao tipo dos valores armazenados, o nome da variável e informamos uma quantidade de elementos entre colchetes:

```
tipo NOME[TAMANHO];
```

Por exemplo, para declarar uma array que irá conter cinco elementos do tipo inteiro, nós escrevemos:

```
int minha_array[5];
```

Também é possível inicializar a array informando uma lista de valores entre chaves e separados por vírgula:

```
int minha_array[5] = {4, 5, 6, 7, 8};
```

Mas, se a array for declarada com a inicialização de seus valores, não é necessário informar a quantidade de elementos, visto que ele será definido a partir do número de elementos da lista:

```
// Isso resulta numa array de 6 elementos...
int minha_array[] = {4, 5, 6, 7, 8, 9};
```

Após a incialização (ou posteriormente, após a atribuição), os valores poderão ser acessados e alterados através da indicação do índice do elemento. Os índices são atribuídos aos valores numa sequência iniciada por zero (`0`) respeitando a ordem de aparição de cada valor na lista. Sendo assim...

```
int minha_array[] = {4, 5, 6, 7, 8, 9};
//       ÍNDICES --> 0  1  2  3  4  5
```

Vamos conferir como isso funciona no exemplo abaixo:

**Exemplo 17**

```
#include <stdio.h>

int main() {

    float valor[] = {10.5, 9.8, 15.7, 13.25};
    int i;

    for (i = 0; i <= 3; i++) {
        printf(
            "Valor %d: R$ %5.2f - Índice %d\n",
            i + 1,
            valor[i],
            i
            );
    }
    return 0;
}
```

Aqui, nós estamos percorrendo cada índice (`i`) com o loop `for` e imprimindo seu valor e o conteúdos dos elementos correspondentes na array `valor`. Portanto...

```
Valor 1: R$ 10.50 - Índice 0
Valor 2: R$  9.80 - Índice 1
Valor 3: R$ 15.70 - Índice 2
Valor 4: R$ 13.25 - Índice 3
```

### 2.9.1 - Arrays bidimensionais

Nos exemplos anteriores, cada índice sempre aponta para um único valor. Esse tipo de organização também é chamado de **vetor** e, se pensarmos numa tabela, seria o equivalente a trabalharmos com uma linha de dados:

```
| idx |   0   |   1   |   2   |   3   |
|-----+-------+-------+-------+-------|
| vlr |  10.5 |   9.8 |  15.7 | 13.25 |
```

Analogamente, é possível criar arrays contendo várias linhas de dados, por exemplo, imagine que queremos registrar as notas de 5 alunos em 4 turmas diferentes:

```
| idx |   0   |   1   |   2   |   3   |   4   | <---- alunos
|-----+-------+-------+-------+-------|-------|
|  0  |  10.0 |   9.5 |   5.5 |   7.5 |   8.0 | --+-- turmas
|  1  |   7.0 |   8.0 |  10.0 |   8.5 |   7.5 |   |
|  2  |   8.5 |   6.5 |   8.0 |  10.0 |   9.0 |   |
|  3  |   9.0 |   8.5 |   6.5 |   9.5 |  10.0 | --+
```

Cada turma desta tabela é uma linha, e os dados dos seus respectivos alunos estariam dispostos nas colunas. Assim, a nota do aluno 2 da turma 3 seria 6.5, por exemplo.

Voltando à linguagem C, isso seria uma matriz bidimensional e ela poderia ser escrita da seguinte forma:

**Exemplo 18**

```
#include <stdio.h>

int main() {
    int turma, aluno;
    float notas[4][5] = {
                          { 10, 9.5, 5.5, 7.5,   8},
                          {  7,   8,  10, 8.5, 7.5},
                          {8.5, 6.5,   8,  10,   9},
                          {  9, 8.5, 6.5, 9.5,  10}
                        };

    printf("\n Turma Aluno1 Aluno2 Aluno3 Aluno4 Aluno5\n");

    for (turma = 0; turma <= 3; turma++) {

        printf("%4d   ", turma + 1);

        for (aluno = 0; aluno <= 4; aluno++) {

            printf("%5.1f  ", notas[turma][aluno]);

        }

        printf("\n");
    }

    printf("\n");

    return 0;
}
```

Com a seguinte saída...

```
 Turma Aluno1 Aluno2 Aluno3 Aluno4 Aluno5
   1    10.0    9.5    5.5    7.5    8.0 
   2     7.0    8.0   10.0    8.5    7.5 
   3     8.5    6.5    8.0   10.0    9.0 
   4     9.0    8.5    6.5    9.5   10.0 
```

Na prática, arrays bidimensionais, também chamadas de **matrizes**, são coleções de vetores.

**Importante!** Se a matriz não for inicializada na declaração, os valores deverão ser atribuídos obrigatoriamente com a especificação dos dois índices da sua localização (linha e coluna). O mesmo vale para eventuais alterações nos valores. Por exemplo:

**Exemplo 19**

```
#include <stdio.h>

int main() {

    int linha, coluna, contador;
    int dezenas[10][10];

    contador = 1;

    for (linha = 0; linha <= 9; linha++) {

        for (coluna = 0; coluna <= 9; coluna++) {

            // Escrevendo valores na array...
            dezenas[linha][coluna] = coluna + contador;


            // Lendo valores da array...
            if (dezenas[linha][coluna] == 100) dezenas[linha][coluna] = 0;
            printf("%02d ", dezenas[linha][coluna]);

        }

        contador += 10;

        puts("");

    }

    return 0;
}
```

O que exibe na saída...

```
:~$ ./exemplo18 
01 02 03 04 05 06 07 08 09 10 
11 12 13 14 15 16 17 18 19 20 
21 22 23 24 25 26 27 28 29 30 
31 32 33 34 35 36 37 38 39 40 
41 42 43 44 45 46 47 48 49 50 
51 52 53 54 55 56 57 58 59 60 
61 62 63 64 65 66 67 68 69 70 
71 72 73 74 75 76 77 78 79 80 
81 82 83 84 85 86 87 88 89 90 
91 92 93 94 95 96 97 98 99 00 
```

### 2.9.2 - Arrays de caracteres e strings

Como vimos, as strings são, na verdade, arrays de caracteres cujo último elemento é o caractere nulo (`\0`). Portanto, a simples declaração de uma string é uma declaração de uma array de caracteres e está sujeita as mesmas regras e comportamentos:

**Exemplo 20**

```
#include <stdio.h>

int main() {

    char letra[11];
    int contador;

    for (contador = 0; contador <= 9; contador++) {

        // Escrevendo na array...
        letra[contador] = 65 + contador;

        // Lendo a array...
        printf("%c\n", letra[contador]);

    }

    // Escrevedo o caractere nulo...
    letra[10] = '\0';

    // Exibindo a string...
    puts(letra);

    return 0;

}
``` 

Sendo assim, uma array unidimensional de strings é uma array bidimensional de caracteres:

**Exemplo 21**

```
#include <stdio.h>

int main() {

    char str[3][11];
    int linha, coluna, contador;

    contador = 65;

    for (linha = 0; linha <= 2; linha++) {

        for (coluna = 0; coluna <= 10; coluna++) {

            if (coluna == 10) {

                str[linha][coluna] = '\0';

            } else {

                str[linha][coluna] = contador;
                contador++;

                if (contador > 90) {

                    str[linha][coluna + 1] = '\0';
                    break;

                }

            }

        }

        puts(str[linha]);

    }

    return 0;

}
```

O que exibirá três strings na saída...

```
ABCDEFGHIJ
KLMNOPQRST
UVWXYZ
```

> **Nota:** Obviamente, não precisamos escrever caractere por caractere em uma array do tipo `char`. Estes últimos exemplos servem apenas para demonstrar o fato de que as strings são arrays de caracteres. Na prática, o processo pode ser muito mais simples:

**Exemplo 22**

```
#include <stdio.h>

int main() {

    int i;
    char str[3][10] = {
        "banana",
        "laranja",
        "abacate"
    };

    for (i = 0; i <= 2; i++) {
        puts(str[i]);
    }

    return 0;

}

```

Na saída...

```
banana
laranja
abacate
```

### 2.9.3 - Utilizando variáveis para definir o tamanho da array

Antes do padrão C99, não era permitido utilizar variáveis para determinar o tamanho de uma array, o precisava ser feito com uma constante criada atarvés da diretiva `#define`. Nós ainda não falamos sobre **constantes** nem diretivas, mas aqui está um exemplo de como isso era feito:

```
#define TAMANHO 10
 
int main()
{
    int arr[TAMANHO];

    // ...

    return 0;
}
```

Os compiladores modernos, porém, permitem a utilização de variáveis para o mesmo fim:

```
int main()
{
    int tamanho = 10;
    int arr[tamanho];

    // ...

    return 0;
}
```

### 2.9.4 - Obtendo o tamanho de uma array

O operador `sizeof`, quando utilizado com uma array, retornará a quantidade de bytes reservado para todos os seus elementos. Como todos os elementos de uma array são sempre do mesmo tipo, dividir o tamanho total em bytes pelo tamanho em bytes do primeiro elemento (já que não sabemos o total ainda) nos dará a quantidade de elementos:

**Exemplo 23**

```
#include <stdio.h>

int main() {

    int i;

    char str[][10] = {
        "banana",
        "laranja",
        "abacate",
        "abacaxi"
    };

    int tamanho = sizeof(str) / sizeof(str[0]);

    printf("A array tem %d elementos...\n", tamanho);

    for (i = 0; i <= (tamanho - 1); i++) {
        puts(str[i]);
    }

    return 0;

}
```

Neste caso, a saída seria...

```
A array tem 4 elementos...
banana
laranja
abacate
abacaxi
```

Com alguma adaptação, este também pode ser um bom método procedural para obter o número de caracteres de uma string:

**Exemplo 24**

```
#include <stdio.h>

int main() {

    char str[] = "banana";

    int tamanho = (sizeof(str) / sizeof(str[0])) - 1;

    printf("A string '%s' tem %d caracteres...\n", str, tamanho);

    return 0;

}
```

Retornando na saída...

```
A string 'banana' tem 6 caracteres...
```


