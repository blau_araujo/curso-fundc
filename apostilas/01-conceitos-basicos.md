# Fundamentos da Linguagem C: 1 - Conceitos Básicos

> *O objetivo deste curso fundamental não é formar programadores profissionais da noite para o dia, mas apresentar os elementos essenciais da programação na linguagem C de forma clara e objetiva, na esperança de que cada um tenha condições de trilhar o seu próprio caminho pisando em bases sólidas.*

## 1.1 - Introdução

A maioria dos autores costuma caracterizar o C como uma linguagem flexível, portável e eficiente[1].

* Flexível, porque permite que ela seja utilizada em diversos tipos de aplicações com todo nível de complexidade imaginável.

* Portável, porque os programas podem ser compilados e utilizados em praticamente qualquer plataforma quase sem alterações.

* Eficiente, porque produz programas rápidos e eficientes no uso da memória.

Aliás, há quem defina resumidamente a linguagem C como uma forma de manipular a memória através de funções e variáveis[2], e esta característica pode ficar bastante evidente mesmo ao longo de um curso básico como o nosso.

## 1.2 - Linguagens interpretadas e compiladas

Outra característica da linguagem C, é que ela é uma linguagem **compilada**. Isso significa que, antes executarmos os nossos programas, eles precisam ser traduzidos para uma linguagem que o computador entenda e ele mesmo possa interpretar. Este processo é chamado de **compilação**.

Outras linguagens, como PHP e o Python por exemplo, são executados ainda sob a forma de um script, e dependem de um interpretador para serem entendidos pela máquina. 

## 1.3 - O compilador GNU/gcc

Para compilarmos os nossos códigos em C, nós vamos precisar de um compilador, e existem várias opções. Neste curso, nós utilizaremos o `gcc` (*GNU Compiler Collection*), originalmente criado para ser o compilador do sistema operacional GNU e, portanto, desenvolvido com o objetivo de ser um software 100% livre.

Como o objetivo de uma compilação é gerar um arquivo capaz de ser executado pela máquina (também chamado de "binário"), é lógico imaginar que existem diversos alvos possíveis, tanto em termos de arquitetura (tipo do processador) quanto de sistema operacional. Aqui, nosso alvo será a arquitetura x86_64 (amd64) em ambientes GNU/Linux.

A sintaxe geral do compilador `gcc` é:

```
gcc [OPÇÕES] [ARQUIVOS FONTE] [ARQUIVOS DE OBJETO] [-o BINÁRIO]
```

Por exemplo, para compilar o código em um arquivo chamado `ola-mundo.c`, basta executar:

```
:~$ gcc ola-mundo.c -o ola-mundo
```

Assim, o código no arquivo `ola-mundo.c`, que é um arquivo comum de texto, será traduzido para a linguagem da máquina, e o binário executável `ola-mundo` será gerado:

```
:~$ ls
ola-mundo  ola-mundo.c
```

Para executar o binário gerado, basta informar seu local e seu nome na linha de comando. No nosso exemplo, nós já estamos na pasta onde o arquivo foi gerado, então só precisamos incluir `./` antes de seu nome:

```
:~$ ./ola-mundo 
Olá, mundo!
```

## 1.4 - A estrutura básica de um programa

Mas nós começamos pelo final! É preciso existir um código antes de algo ser compilado. O código é escrito em um ou mais arquivos de texto plano (*raw text*, em inglês) e podemos utilizar qualquer editor que não inclua formatações para escrevê-lo. Os arquivos do código geralmente levam a extensão `.c` ou `.h`, neste caso, quando são arquivos de cabeçalho (*header*, em inglês).

Usando o nosso programa `ola-mundo` como exemplo, este seria seu código no arquivo `ola-mundo.c`:

```
1    #include <stdlib.h>
2    #include <stdio.h>
3    
4    int main() {
5        printf("Olá, mundo!\n");
6        return 0;
7    }
```

Nas duas primeiras linhas, nós temos a diretiva `#include`, que faz a inclusão dos arquivos de cabeçalho que serão utilizados na compilação dos nossos programas.

```
1    #include <stdlib.h>
2    #include <stdio.h>
```

Os arquivos de cabeçalho são chamados de **bibliotecas**, e contém diversas coleções de código que podem ser reaproveitados, evitando que precisemos "reinventar a roda" a cada novo programa.

No programa `ola-mundo`, nós incluímos as bibliotecas `stdlib` e `stdio`, ambas fazem parte do conjunto de bibliotecas C compartilhadas GNU (`libc`), e estão disponíveis por padrão na maioria das distribuições Linux.

## 1.5 - Bibliotecas 'stdlib' e 'stdio'

O cabeçalho `stdlib.h` (de **standard library**) é a biblioteca de propósito geral da linguagem C. Ela possui componentes (chamados de **membros**) para diversas finalidades, como alocação de memória, controle de processos, conversões, entre outras.

Mesmo sendo fundamental para muitas operações básicas na linguagem C, a biblioteca `stdlib` pode não conter tudo de que o nosso programa precisa para compilar. Aqui mesmo, no nosso exemplo, nós chamamos a função `printf` (linha 5), que serve para exibir mensagens formatadas na saída padrão (o terminal), mas `printf` não é membro de `stdlib`, e sim da biblioteca `stdio`, razão pela qual nós tivemos que incluí-la no nosso código.

## 1.6 - A função 'main'

Um outro elemento essencial da estrutura de um código em C é a função `main`:

```
4    int main() {
5        printf("Olá, mundo!\n");
6        return 0;
7    }
```

Nós teremos um tópico exclusivo para falarmos de funções, mas você pode entendê-las, de forma simplificada, como um bloco do código que agrupa e identifica um conjunto de instruções. Então, toda função terá um nome pelo qual poderá ser chamada, um conjunto de instruções e retornará um valor.

> **Importante!** A função `main` é chamada automaticamente e, sem ela, o programa não pode ser executado.

## 1.7 - Retornos

Outro detalhe importante sobre as funções, é que o sistema operacional deve estar preparado para receber seus valores retornados na memória. Por esse motivo, logo antes do nome da função, nós temos que indicar o tipo de valor que será retornado, já que isso afeta a quantidade de bytes que o sistema deverá reservar na memória.

```
4    int main() {
         ...
6        return 0;
7    }
```

No nosso programa `ola-mundo`, repare que a instrução `return` indica que o valor retornado será `0`, um número inteiro. Portanto, ao declarar a função, nós incluímos a palavra chave `int` antes de seu nome, justamente para informar que o valor retornado será um número inteiro.

> **Nota 1:** nós veremos isso melhor no futuro, mas a linha `return 0;` poderia ser dispensada se, em vez de `int` nós declarássemos que o retorno da função seria do tipo `void`.

> **Nota 2:** `void` é considerado um **tipo de dados** apenas para efeito de organização, mas é apenas uma palavra chave que se usa no lugar da declaração do tipo da função para indicar que nada será retornado.

## 1.8 - A função 'printf'

Como dissemos, a função `printf` não existe no nosso código nem é membro da biblioteca padrão (`stdlib`), por isso tivemos que incluir a diretiva:


```
2    #include <stdio.h>
```

Sua principal finalidade é exibir mensagens na saída padrão (`STDOUT`), que é essencialmente a tela do terminal. No nosso caso, nós utilizamos a função `printf` para exibir a mensagem `Olá, mundo!`:

```
5        printf("Olá, mundo!\n");
```

As informações passadas para uma função são chamadas de "argumentos" e eles são escritos entre os parêntesis. Se uma função não espera receber argumentos, ela é chamada apenas por seu nome seguido de parêntesis vazios:

```
função_sem_argumentos();
```

Além do texto a ser exibido, a função `printf` pode receber outros tipos de argumentos, o que nós veremos em detalhes em tópicos futuros. No momento, é importante observar que a mensagem é uma *string* (uma cadeia de caracteres) e é escrita entre aspas.

Como o retorno da função `printf` não inclui uma quebra de linha, nós incluímos o caractere de controle `\n` (*new line*) no final da mensagem, também dentro das aspas.

## 1.9 - Acentuação e a biblioteca 'locale'

As definições de conjuntos de caracteres válidos, formatos de apresentação de números e moedas, bem como outros aspectos da localização dos programas, geralmente são tratados em plataformas GNU/Linux através de algumas variáveis de ambiente (variáveis do shell que estão disponíveis para serem acessadas pelos programas).

Se você está usando alguma distribuição GNU/Linux, é bem provável que você não precise especificar a localização para que os nossos exemplos sejam capazes de exibir acentos corretamente, o próprio compilador é capaz de detectar as configurações de localização do seu sistema. Mas isso pode não ser verdade em outras plataformas (como no Windows).

Se for o caso, nós podemos fazer com que o nosso programa leia as configurações de localização do ambiente utilizando a função `setlocale` da biblioteca `locale.h`:

```
 1   #include <stdlib.h>
 2   #include <stdio.h>
 3   #include <locale.h>
 4   
 5   int main() {
 6       setlocale(LC_ALL, "")
 7
 8       printf("Olá, mundo!\n");
 9       return 0;
10   }
```

A função `setlocale` espera dois argumentos: uma constante indicando a categoria que será afetada e um valor que representa a localização. No caso, a constante `LC_ALL` indica que queremos definir todas as configurações de localização. Utilizando aspas vazias (`""`) no lugar do valor, nós informamos que as configurações de localização da plataforma deverão ser lidas e aplicadas na compilação, o que nos permitirá utilizar caracteres acentuados e outras definições regionais.

## 1.10 - Olá, mundo!

Existem mais coisas acontecendo no nosso programa `ola-mundo` e muitos outros elementos da estrutura básica de um programa em C ficaram de fora. Mas este é só o começo, e nós teremos muitas oportunidades de conhecer e praticar os principais fundamentos da linguagem C.

### Referências e fontes

* [1] Sílvio do Lago Pereira - "Linguagem C"
* [2] Alexandre Fiori - "O Fantástico Mundo da Linguagem C"
