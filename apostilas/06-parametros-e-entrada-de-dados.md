# 6 - Parâmetros posicionais e entrada de dados

Na linguagem C, os parâmetros de execução passados na linha de comando do shell são capturados pela função `main()` através de dois argumentos: a variável `argc`, que armazena um inteiro correspondente ao número total de parâmetros, e `*argv[]` que captura os parâmetros como uma array de ponteiros para strings.

Esses argumentos são declarados na função `main()` da seguinte forma:

```
int main(int argc, char *argv[]) {
    ...
    return 0;
} 
```

Antes de prosseguirmos, porém, precisamos entender mais alguns detalhes sobre como as arrays e strings se relacionam com os ponteiros.

## 6.1 - Ponteiros e arrays

Os elementos de uma array são armazenados em espaços vizinhos na memória. Por exemplo, vamos considerar uma array com quatro elementos do tipo inteiro (ocupando 4 bytes cada):

```
int arr[4] = {1, 2, 3, 4};
```

Na memória, supondo que o primeiro elemento foi armazenado no endereço hipotético `0x1000`, os demais elementos seriam armazenados como no esquema abaixo:

```
     0x1000  0x1004  0x1008  0x100C
    +-------+-------+-------+-------+
    |   1   |   2   |   3   |   4   |
    +-------+-------+-------+-------+
     arr[0]  arr[1]  arr[2]  arr[3]
```

Nós podemos ter uma visão mais prática do que acontece observando o exemplo abaixo:

**Exemplo 29**

```
include <stdio.h>

int main() {

    int i, arr[5] = {1, 2, 3, 4, 5};

    for (i = 0; i < 5; i++) {
        printf("O valor de arr[%d] é %d e está no endereço %p\n",
              i,
              arr[i],
              &arr[i]
              );
    }

    return 0;
}
```

O que imprimiria na saída...

```
O valor de arr[0] é 1 e está no endereço 0x7ffc138fad10
O valor de arr[1] é 2 e está no endereço 0x7ffc138fad14
O valor de arr[2] é 3 e está no endereço 0x7ffc138fad18
O valor de arr[3] é 4 e está no endereço 0x7ffc138fad1c
O valor de arr[4] é 5 e está no endereço 0x7ffc138fad20
```

Na verdade, por ser mais eficiente, o compilador irá tratar o nome de uma array como um **ponteiro constante** que aponta para o endereço do seu primeiro elemento (`*arr`), deduzindo os endereços seguintes a partir do tamanho em bytes do tipo de dado armazenado nos elementos.

Uma coisa interessante, que será vista em detalhes em tópicos futuros, é que, na aritmética dos ponteiros, quando fazemos uma soma, nós estamos fazendo com que ele dê um "salto" e aponte para outro endereço de memória. O tamanho desse "salto" irá depender do tipo do ponteiro e do valor somado.

Sendo assim, considerando que `*arr` equivale a `arr[0]`, nós podemos dizer, de forma mais geral, que `*(arr+i)` é o mesmo que `arr[i]`, o que nos permite acessar os elementos e os endereços de uma array como no exemplo abaixo:

**Exemplo 30**

```
#include <stdio.h>

int main() {

    int i, arr[5] = {1, 2, 3, 4, 5};

    for (i = 0; i < 5; i++) {
        printf("O valor de arr[%d] é %d e está no endereço %p\n",
              i,
              *(arr + i),
              arr + i
              );
    }

    return 0;
}
```

E a saída seria...

```
O valor de arr[0] é 1 e está no endereço 0x7ffcaadccf50
O valor de arr[1] é 2 e está no endereço 0x7ffcaadccf54
O valor de arr[2] é 3 e está no endereço 0x7ffcaadccf58
O valor de arr[3] é 4 e está no endereço 0x7ffcaadccf5c
O valor de arr[4] é 5 e está no endereço 0x7ffcaadccf60
```

Mas, como dissemos: o nome de uma array é um **ponteiro constante** que aponta para o endereço do seu primeiro elemento. Isso significa que ele não pode ser alterado (reatribuído) para apontar para outro endereço na memória. Por outro lado, um ponteiro normal permitiria esse tipo de operação. Portanto, algo assim seria possível:

**Exemplo 31**

```
#include <stdio.h>

int main() {

    int i, *p, arr[5] = {1, 2, 3, 4, 5};

    p = arr;

    for (i = 0; i < 5; i++) {

        printf("arr[%d] = %d - %p\n", i, *p, p);

        p++;

    }

    return 0;
}

```

O que exibiria...

```
arr[0] = 1 - 0x7ffeefe2c500
arr[1] = 2 - 0x7ffeefe2c504
arr[2] = 3 - 0x7ffeefe2c508
arr[3] = 4 - 0x7ffeefe2c50c
arr[4] = 5 - 0x7ffeefe2c510
```

Da mesma forma, uma array bidimensional, que pode ser entendida como uma array de arrays, também pode ser expressa como um ponteiro constante. Ou seja, se antes nós vimos que `*(arr+i)` é o mesmo que `arr[i]`, `*(*(arr+i)+j)` é o mesmo que `arr[i][j]`. Vejamos na prática:

**Exemplo 32**

```
#include <stdio.h>

int main() {

    int arr[3][4] = {
                        {11, 12, 13, 14},
                        {21, 22, 23, 24},
                        {31, 32, 33, 34}
                    };

    int i, j;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 4; j++) {
            printf("arr[%d][%d] = %d - %p\n",
            i, j, *(*(arr + i)+j), ((arr + i) + j));
        }
        puts("");
    }

    return 0;
}
```

Isso produziria a seguinte saída:

```
arr[0][0] = 11 - 0x7ffc1af81760
arr[0][1] = 12 - 0x7ffc1af81770
arr[0][2] = 13 - 0x7ffc1af81780
arr[0][3] = 14 - 0x7ffc1af81790

arr[1][0] = 21 - 0x7ffc1af81770
arr[1][1] = 22 - 0x7ffc1af81780
arr[1][2] = 23 - 0x7ffc1af81790
arr[1][3] = 24 - 0x7ffc1af817a0

arr[2][0] = 31 - 0x7ffc1af81780
arr[2][1] = 32 - 0x7ffc1af81790
arr[2][2] = 33 - 0x7ffc1af817a0
arr[2][3] = 34 - 0x7ffc1af817b0
```

## 6.2 - Array de ponteiros para strings

Normalmente, uma array de strings deve ser declarada informando um comprimeto máximo em caracteres que todas os elementos poderiam conter. Por exemplo:

```
char frutas[3][20] = {"banana", "laranja", "abacate"};
```

Isso obrigaria o nosso programa a reservar um espaço de 60 bytes na memória, mas apenas 23 bytes estariam sendo efetivamente utilizados pelas strings.

Uma array de ponteiros para strings é uma array onde cada ponteiro aponta para o primeiro caractere de uma string ou para o endereço inicial (base) de uma string. 
As arrays de ponteiros para strings são inicializadas da seguinte forma:

```
char *strings[MAX] = {"str_1", "str_2", ..., "str_MAX"};
```

Ou, apenas na inicialização...

```
char *strings[] = {"str_1", "str_2", ..., "str_n"};
```

Por exemplo:

```
char *frutas[] = {"banana", "laranja", "abacate"};
```

Desta forma, a organização das strings na memória seria algo assim:

```
           frutas = array de ponteiros
             |
             V     0    1    2    3    4    5    6    7
           +-----+----+----+----+----+----+----+----+
frutas[0]  |1000 | b  | a  | n  | a  | n  | a  | \0 |
           +-----+----+----+----+----+----+----+----+----+
frutas[1]  |1008 | l  | a  | r  | a  | n  | j  | a  | \0 |
           +-----+----+----+----+----+----+----+----+----+
frutas[2]  |1010 | a  | b  | a  | c  | a  | t  | e  | \0 |
           +-----+----+----+----+----+----+----+----+----+
```

No diagrama, a primeira coluna é a array de ponteiros `frutas`, onde cada um de seus elementos aponta para o endereço do primeiro caractere de uma string literal.

Isso representa uma economia de memória, mas a diferença mais interessante de uma array de strings para uma array de ponteiros para strings é que, com uma array de ponteiros, nós podemos alterar as strings nos elementos! Veja no exemplo:

**Exemplo 33**

```
#include <stdio.h>

int main() {

    char *frutas[] = {
        "banana da terra",
        "laranja",
        "abacate"
    };
    int i;

    for (i = 0 ; i < 3; i++ ) {
        printf("%s - %p\n",
        frutas[i],
        &frutas[i]
        );
    }

    frutas[0] = "pera";
    frutas[1] = "uva";
    frutas[2] = "limão";

    puts("");

    for (i = 0 ; i < 3; i++ ) {
        printf("%s - %p\n",
        frutas[i],
        &frutas[i]
        );
    }

    return 0;
}
```

Cuja saída seria...

```
banana da terra - 0x7ffde00247a0
laranja - 0x7ffde00247a8
abacate - 0x7ffde00247b0

pera - 0x7ffde00247a0
uva - 0x7ffde00247a8
limão - 0x7ffde00247b0
```

Esse mesmo princípio pode ser aplicado a ponteiros de strings em geral:

**Exemplo 34**

```
#include <stdio.h>

int main() {

    char *teste = "Olá, mundo";

    puts(teste);

    teste = "Adeus, mundo!";

    puts(teste);

    return 0;
}
```

O que exibiria...

```
Olá, mundo
Adeus, mundo!
```

## 6.3 - De volta aos parâmetros posicionais

Como vimos no início, os valores passados na linha de comando são capturados pela array que aparece nos argumentos da função `main()` como `char *argv[]`, o que significa que `argv` é uma array de ponteiros para strings.

Então, como podemos deduzir, `argv` será inicializado no momento que a função `main()` for chamada, e isso fará com que as strings passadas na linha de comando estejam disponíveis para serem utilizadas no nosso programa.

Mesmo que nenhum parâmetro seja passado, `argv[0]` irá armazenar o nome do programa chamado no prompt de comandos. Sendo assim, fica claro que o valor mínimo de `argc` será `1`.

Os parâmetros devem ser passados na linha de comando separados por espaços ou tabulações e podem ser agrupados entre aspas para compor uma string contendo espaços.

**Exemplo 35**

```
#include <stdio.h>

int main( int argc, char *argv[] ) {
    int n;

    for (n = 0; n < argc; n++) {
        printf("argv[%d] = %s\n", n, argv[n]);
    }

    return 0;
}
```

Testando...

```
:~$ ./exemplo35 a b c
argv[0] = ./exemplo35
argv[1] = a
argv[2] = b
argv[3] = c

:~$ ./exemplo35 a 'b c'
argv[0] = ./exemplo35
argv[1] = a
argv[2] = b c
```

## 6.4 - Convertendo strings em números

Como todos os parâmetros são passados para o programa na forma de strings, os valores numéricos terão que ser convertidos para seus respectivos tipos, o que pode ser feito com as funções `atoi()` (inteiro) e `atof()` (ponto flutuante), ambas em `stdlib.h`.

**Exemplo 36**

```
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] ) {

    if (argc == 3) {
        printf("argv[1] x argv[2] = %d\n",
                atoi(argv[1]) * atoi(argv[2])
                );
    }

    return 0;
}
```


 
