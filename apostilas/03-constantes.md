# 3 - Constantes

Em contraste com as variáveis, as constantes são elementos da linguagem que expressam valores que não mudam. Sendo assim, tanto os nomes que representam esses valores imutáveis, quanto a própria expressão literal desses valores, são o que nós chamamos de constantes.

Assim como vimos no tópico sobre os tipos, as constantes também podem ser classificadas como **prímárias**, quando relacionadas aos tipos primários, ou **secundárias** (ou "derivadas"), quando expressam valores de tipos derivados. 

Neste tópico, o nosso foco serão as constantes prímárias, a representação literal de strings (uma constante secundária) e a criação de constantes simbólicas. Outras constantes secundárias serão vistas em seus respectivos tópicos.  

## 3.1 - Constantes numéricas

Constantes numéricas são, como podemos deduzir, as representações literais de números que podem ou não conter: pontos decimais, notações de potência de dez, e outros caracteres que indiquem sua base de numeração e o seu tipo. Fora isso, para serem consideradas constantes numéricas, as expressões precisam ter pelo menos um dígito e não podem conter vírgulas ou espaços.

As constantes numéricas podem ser de dois tipos: **constantes inteiras** ou **constantes de ponto flutuante**, também chamadas de constantes numéricas reais.

### 3.1.1 - Constantes inteiras

As constantes inteiras são aquelas representadas por números sem o ponto de separação de casas decimais e elas podem ser escritas nas bases 10 (decimais), 8 (octais) ou 16 (hexadecimais).

| Constantes Inteiras | Representação |
|---------------------|---------------|
| Constantes Decimais | Contêm dígitos de `0` a `9`, mas não podem começar com o `0`. |
| Constantes Octais   | Contêm dígitos de `0` a `7` e devem começar com o `0`. |
| Constantes Hexadecimais | Contêm dígitos de `0` a `9`, letras de `A` a `F` e devem começar com `0x`. |

Seja qual for a base utilizada, o valor sempre será armazenado como uma sequência de dígitos binários na memória. Da mesma forma, não existem representações negativas de constantes numéricas! O sinal de negativo (`-`) diante de um número é, na verdade, o **operador unário menos**, responsável por fazer o compilador calcular o negativo do valor e, este sim, é o que será escrito (em bináro) na memória.

> **Nota:** Na prática, a representação de uma constante numérica só tem efeito sobre como o compilador irá identificar o seu tipo e, consequentemente, determinar quanto de espaço em memória deverá ser alocado para armazená-la.

Por padrão, uma constante inteira será do tipo `int`. Se for grande demais para o espaço que um tipo `int` ocuparia, a constante é promovida para `long int` ou um tipo ainda maior, uma `unsigned long int`, dependendo do caso. Se quisermos especificar o modificador do tipo `int` a ser aplicado, nós podemos utilizar os sufixos `L` (`long`) e `U` (`unsigned`) na representação literal:

```
// Especificando o tipo 'long int':
123l, 123L, 065l, 065L, 0xf0l, 0XF0L, etc...

// Especificando o tipo 'unsigned int':
123u, 123U, 065u, 065U, 0xf0u, 0XF0U, etc...

// Especificando o tipo 'unsigned long int':
123ul, 123UL, 065ul, 065UL, 0xf0ul, 0XF0UL, etc...
```

### 3.1.2 - Constantes de ponto flutuante

Basicamente, o que denota uma constante numérica de ponto flutuante é que ela terá um ponto decimal e/ou uma letra `E` separando a parte significativa (o "coeficiente" ou "mantissa") do expoente da potência de dez.

Na forma fracionária (sem expoente), a constante deve ter pelo menos um dígito e um ponto. Na forma exponencial (notação científica), o expoente da potência de dez é antecedido pela letra `E` (pode ser maiúscula ou minúscula) e deve ter pelo menos um dígito.

Por padrão, constantes de ponto flutuante são idetificadas como sendo do tipo `double`, mas nós podemos especificar o tipo `float` com o sufixo `F`, ou o tipo `long double` com o sufixo `L`. Por exemplo:

```
// Forma fracionária:
12.5, 0.36, .5, etc...

// Forma exponencial:
123e4, 1.234E5, 0.5e-9, etc...

// Especificando o tipo 'float':
1.234F, 567f, 0.987f, etc...

// Especificando o tipo 'long double'...
1.234L, 567l, 3.35e67L, etc... 
```

## 3.2 - Constantes caracteres

Uma constante caractere é representada por uma única letra, dígito ou qualquer símbolo entre aspas simples.

> **Nota:** Entre as aspas simples deve haver apenas um caractere!

Cada constante caractere está associada a um valor inteiro de 1 byte na tabela ASCII, e é este valor que, de fato, é armazenado na memória. Então, quando escrevemos a constante `'A'`, o que o compilador realmente enxerga é o inteiro de 1 byte `64`.

## 3.3 - Constantes strings

A constante string é representada por um ou mais caracteres entre aspas duplas. Internamente, o compilador inclui o caractere nulo `\0` ao final da cadeia.

Como vimos no tópico anterior, a linguagem C não tem dados do tipo string. O que temos são arrays de valores do tipo `char`, e as arrays estão na categoria dos tipos derivados. Portanto, a rigor, as constantes strings também estariam no grupo das constantes secundárias.

## 3.4 - Constantes simbólicas

Da mesma forma que atribuímos valores a variáveis, nós podemos criar nomes que irão "simbolizar" o valor de uma constante, daí o nome "constantes simbólicas". É possível criar constantes simbólicas de duas formas: com a diretiva de pré-processamento `#define` ou com a palavra chave `const`.

### 3.4.1 - A diretiva '#define'

Assim como o `#include`, `#define` é uma **diretiva de pré-processamento**. As diretivas de pré-processamento são instruções para indicar substituições de texto, ou seja, elas instruem o pré-processador da linguagem C a fazer substituições de partes específicas do nosso código antes do processo de compilação.

Como não faz parte do compilador, o pré-processador possui uma sintaxe diferente das instruções normais que o compilador é capaz de reconhecer, por exemplo:

* As instruções começam com uma cerquilha (`#`);
* Cada instrução está restrita a uma linha do código;
* As instruções terminam com uma quebra de linha (não com o `;`).

> **Nota:** Nós teremos um tópico específico para abordarmos as diretivas de pré-processamento. No momento, porém, o que nos interessa é a criação de constantes simbólicas com a diretiva `#define`.

A diretiva `#define` permite a definição de macros (macroinstruções). Como essas macros são definidas fora da parte do nosso código que será vista pelo compilador, elas são imutáveis, e é dessa característica que nós nos aproveitamos para criar as constantes. 

**Nota:** É por isso que encontraremos muitas vezes as constantes sendo chamadas de macros.

A sintaxe para criar uma constante com a diretiva `#define` é:

```
#define NOME VALOR
```

Ou ainda...

```
#define NOME (EXPRESSÃO)

Quando NOME está associado a um VALOR, este VALOR pode ser a representação literal de um número, um caractere ou uma string. Se for uma EXPRESSÃO, a associação será feita com o valor resultante da sua avaliação. 

**NOTA:** Se EXPRESSÃO contiver operadores, ela deverá ser escrita entre parêntesis. 

Exemplos:

```
#define TAXA 1.15
#define LINHA_MAX 25
#define NL '\n'
#define EMAIL "blau@debxp.org"
#define AREA (25 * 12)
```

Também podemos usar constantes nas definições por expressões:

```
#include <stdio.h>

#define RAIO 3.5
#define CIRC 22
#define PI (CIRC / (2* RAIO))

int main() {

    printf("Pi = %.2f", PI);

    return 0;
}

```

O que exibiria na saída...

```
3.14
```

> **Nota:** Os nomes de constantes geralmente são escritos em maiúsculas e as definições geralmente são feitas  no começo do código. Isso dá mais legibilidade e torna a manutenção bem mais fácil.

### 3.4.2 - A palavra-chave 'const'

A função da palavra-chave 'const' é tornar uma variável inalterável (apenas leitura), o que, na prática, é uma constante. Mas, como ainda se trata da definição e inicialização de uma variável, nós temos que obedecer a sintaxe:

```
const TIPO NOME = VALOR;
```

Por exemplo:

```
#include <stdio.h>

int main() {

    const int LARGURA = 30;
    const int ALTURA = 10;

    printf("Área: %d\n", LARGURA * ALTURA);

    return 0;
}
```
