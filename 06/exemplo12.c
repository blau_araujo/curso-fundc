#include <stdio.h>
#include <string.h>

int main() {

    char fruta[15];

    strcpy(fruta, "banana");
    printf("A fruta é %s\n", fruta);

    strcpy(fruta, "abacaxi");
    printf("A fruta é %s\n", fruta);

    strcpy(fruta, "maçã");
    printf("A fruta é %s\n", fruta);

    return 0;
}

