#include <stdio.h>

int main() {

    char fruta[15] = "banana";

    // Aqui não temos quebra de linha...
    fputs("A fruta é: ", stderr);

    // Aqui temos quebra de linha...
    puts(fruta);

    return 0;
}
