#include <stdio.h>
#include <string.h>

int main() {

    char fruta[10];

    strcpy(fruta, "banana");
    printf("A fruta é %s\n", fruta);

    strcpy(fruta, "fruta de pé de mamoeiro");
    printf("A fruta é %s\n", fruta);

    return 0;
}
