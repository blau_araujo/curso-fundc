#include <stdio.h>
#include <string.h>

int main() {

    char fruta[15];

    strcpy(fruta, "banana");
    printf("A fruta era %s.\n", fruta);

    strcat(fruta, " abacaxi laranja");
    printf("A string '%s' ocupa %ld bytes?!\n",
           fruta,
           sizeof(fruta));

    printf("Agora a string tem %ld caracteres!\n",
           strlen(fruta));

    return 0;
}

