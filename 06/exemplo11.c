#include <stdio.h>

int main() {

// Exemplo 11-a

/*
    _Bool estado = 23;

    printf("O estado booleano é: %d\n", estado);

    estado = 0;

    printf("O estado booleano é: %d\n", estado);
*/


// Exemplo 11-b

/*
    bool estado = 234;

    printf("O estado booleano é: %d\n", estado);

    estado = true;

    printf("O estado booleano é: %d\n", estado);

    estado = false;

    printf("O estado booleano é: %d\n", estado);
*/

    return 0;
}
