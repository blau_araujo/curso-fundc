# Exercícios

## 1 - Cálculo dos limites máximo e mínimo do tipo inteiro.

Sabendo que:

* O cabeçalho `math.h` possui a função `pow(X, Y)`, que eleva `X` ao expoente `Y`;
* O limite máximo dos inteiros é `2**((8*TAMANHO)-1) - 1`;
* O limite mínimo dos inteiros é `-2**((8*TAMANHO)-1)`;
* O tamanho em bytes do tipo pode ser obtido com o operador `sizeof()`...

Crie um programa que produza a seguinte saída no terminal **sem utilizar o cabeçalho `limits.h`**:

```
Tamanho em bytes: 4
Inteiro máximo: 2147483647
Inteiro mínimo: -2147483648
```

## 2 - Acendendo LEDs

Suponha que você tenha uma fileira de **8 LEDs** controlada por um dispositivo qualquer. Suponha também que basta
enviar um byte (8 bits) para a saída desse controlador para que os LEDs que receberem o bit 1 sejam acesos e os que
receberem o bit 0 se apaguem. Levando em conta a tabela abaixo, crie um programa que envie em sequência os caracteres
necessários para que os LEDs sejam acesos em sequência.

**Observação 1**: represente a saída do seu programa em base 16 (hexa).

**Observação 2**: o especificador de formato para exibir números hexadecimais com 2 dígitos é `%02x`.


**Tabela**

| Byte      | Hex | Dec |
|-----------|:---:|----:|
| 1000 0000 | 80  | 128 |
| 0100 0000 | 40  |  64 |
| 0010 0000 | 20  |  32 |
| 0001 0000 | 10  |  16 |
| 0000 1000 | 08  |   8 |
| 0000 0100 | 04  |   4 |
| 0000 0010 | 02  |   2 |
| 0000 0001 | 01  |   1 |
| 0000 0000 | 00  |   0 |

## 3 - Conversão de medidas

Sabendo que:

| Unidade    | Metros |
|------------|--------|
| 1 milha    | 1760   |
| 1 jarda    | 0.9144 |
| 1 pé       | 0.3048 |
| 1 polegada | 0.0254 |

Crie um programa que converta medidas nas unidades da coluna da esquerda para as da coluna da esquerda e vice versa.

Os dados serão passados por parâmetros na linha de comando no seguinte formato:

```
:~$ converter MEDIDA UNIDADE_ORIGINAL [UNIDADE_DESTINO]
```

Se UNIDADE_DESTINO não for informada, a conversão será feita para **metros**.

**Importante!** A precisão das medidas deve ser de 4 casas decimais!

**Dica:** Para receber parâmetros da linha de comando, a função `main()` deve ser declarada da seguinte forma:

```
int main(int argc, char **argv) {
    ...
}
```

Onde:

* `argc` receberá a quantidade de parâmetros
* `argv` é uma array que receberá os parâmetros como strings




