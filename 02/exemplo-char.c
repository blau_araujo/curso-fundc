#include <stdio.h>

int main() {

    char c[] = "Á";

    printf("Caractere          : %s\n", c);
    printf("Caractere (decimal): %d\n", c);
    printf("Caractere (octal)  : %o\n", c);
    printf("Caractere (hexa)   : %x\n", c);

    return 0;
}
