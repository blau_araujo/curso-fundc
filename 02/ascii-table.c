#include <stdio.h>

int main() {

    int c;

    printf("\nTabela ASCII Padrão (ANSI-C)\n\n");

    for (c = 0; c <= 255; c++) {
        printf("Decimal: %d - Caractere: %c\n", c, c);
    }

    return 0;
}
