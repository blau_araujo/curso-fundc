#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] ) {

    if (argc == 3) {
        printf("argv[1] x argv[2] = %f\n",
                atof(argv[1]) * atoi(argv[2])
                );
    }

    return 0;
}
