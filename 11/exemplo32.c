#include <stdio.h>

int main() {

    char arr[3][10] = {
        "abcd",
        "efgh",
        "ijkl"
    };

    int i, j;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 10; j++) {
            printf("arr[%d][%d] = %c - %p\n",
            i, j, *(*(arr + i)+j), ((arr + i) + j));
        }
        puts("");
    }

    return 0;
}
