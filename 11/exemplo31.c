#include <stdio.h>

int main() {

    int i, *p, arr[5] = {1, 2, 3, 4, 5};

    p = arr;

    for (i = 0; i < 5; i++) {

        printf("arr[%d] = %d - %p\n", i, *p, p);

        p++;

    }

    return 0;
}
