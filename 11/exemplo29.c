#include <stdio.h>

int main() {

    int i, arr[5] = {1, 2, 3, 4, 5};

    for (i = 0; i < 5; i++) {
        printf("O valor de arr[%d] é %d e está no endereço %p\n",
              i,
              arr[i],
              &arr[i]
              );
    }

    return 0;
}
