#include <stdio.h>

int main() {

    char *frutas[] = {
        "banana da terra",
        "laranja",
        "abacate"
    };
    int i;

    for (i = 0 ; i < 3; i++ ) {
        printf("%s - %p\n",
        frutas[i],
        &frutas[i]
        );
    }

    frutas[0] = "pera";
    frutas[1] = "uva";
    frutas[2] = "limão";

    puts("");

    for (i = 0 ; i < 3; i++ ) {
        printf("%s - %p\n",
        frutas[i],
        &frutas[i]
        );
    }

    return 0;
}
