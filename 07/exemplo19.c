#include <stdio.h>

int main() {

    int linha, coluna, contador;
    int dezenas[10][10];

    contador = 1;

    for (linha = 0; linha <= 9; linha++) {

        for (coluna = 0; coluna <= 9; coluna++) {

            dezenas[linha][coluna] = coluna + contador;

            if (dezenas[linha][coluna] == 100) dezenas[linha][coluna] = 0;

            printf("%02d ", dezenas[linha][coluna]);

        }

        contador += 10;

        puts("");

    }

    return 0;
}
