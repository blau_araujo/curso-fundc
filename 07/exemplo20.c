#include <stdio.h>

int main() {

    char letra[11];
    int contador;

    for (contador = 0; contador <= 9; contador++) {

        // Escrevendo na array...
        letra[contador] = 65 + contador;

        // Lendo a array...
        printf("%c\n", letra[contador]);

    }

    // Escrevedo o caractere nulo...
    // letra[10] = '\0';

    // Exibindo a string...
    puts(letra);

    return 0;

}
