#include <stdio.h>

int main() {
    int turma, aluno;
    float notas[4][5] = {
                          { 10, 9.5, 5.5, 7.5,   8},
                          {  7,   8,  10, 8.5, 7.5},
                          {8.5, 6.5,   8,  10,   9},
                          {  9, 8.5, 6.5, 9.5,  10}
                        };

    printf("\n Turma Aluno1 Aluno2 Aluno3 Aluno4 Aluno5\n");

    for (turma = 0; turma <= 3; turma++) {

        printf("%4d   ", turma + 1);

        for (aluno = 0; aluno <= 4; aluno++) {

            printf("%5.1f  ", notas[turma][aluno]);

        }

        printf("\n");
    }

    printf("\n");

    return 0;
}
