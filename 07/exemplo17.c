#include <stdio.h>

int main() {

    float valor[] = {10.5, 9.8, 15.7, 13.25};
    int i;

    for (i = 0; i <= 3; i++) {
        printf(
            "Valor %d: R$ %5.2f - Índice %d\n",
            i + 1,
            valor[i],
            i
            );
    }
    return 0;
}

