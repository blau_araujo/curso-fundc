#include <stdio.h>

int main() {

    char str[] = "maçã";

    int tamanho = (sizeof(str) / sizeof(str[0])) - 1;

    printf("A string '%s' tem %d caracteres...\n", str, tamanho);

    return 0;

}
