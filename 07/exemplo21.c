#include <stdio.h>

int main() {

    char str[3][11];
    int linha, coluna, contador;

    contador = 65;

    for (linha = 0; linha <= 2; linha++) {

        for (coluna = 0; coluna <= 10; coluna++) {

            if (coluna == 10) {
                str[linha][coluna] = '\0';
            } else {
                str[linha][coluna] = contador;
                contador++;

                if (contador > 90) {
                    str[linha][coluna + 1] = '\0';
                    break;
                }

            }

        }

        puts(str[linha]);

    }

    return 0;

}
