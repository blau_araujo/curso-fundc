#include <stdio.h>

int main() {

    int i;

    char str[][10] = {
        "banana",
        "laranja",
        "abacate",
        "abacaxi"
    };

    int tamanho = sizeof(str) / sizeof(str[0]);

    printf("A array tem %d elementos...\n", tamanho);

    for (i = 0; i <= (tamanho - 1); i++) {
        puts(str[i]);
    }

    return 0;

}
