#include <stdio.h>

int main() {

    // Caractere normal...

    /*
    char c;

    printf("Tecle algo: ");

    c = getchar();

    printf("Você digitou: %c\n", c);
    */

    // Caractere multibyte...

    
    char c[3];
    int i;

    printf("Tecle algo: ");

    for (i = 0; i < 2; i++) {
        c[i] = getchar();
    }
    c[2] = '\0';

    printf("Você digitou: %s\n", c);
    

    return 0;
}
