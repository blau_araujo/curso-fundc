#include <stdio.h>
#include <string.h>

// Máximo de caracteres incluindo '\0'...

#define MAX 21

int main() {

    char str[MAX];

    printf("Digite algo (até %d caracteres): ", MAX);

    /*
    Se a string lida for nula, a subtração de
    menos 1 do comprimento da string resultará
    em um valor positivo enorme fora dos limites
    da array.
    */

    if (fgets(str, MAX, stdin) == NULL) {
        printf("Erro na leitura!\n");
    } else {

        /*
        strchr() tenta encontrar a primeira
        ocorrência de um caractere e retorna
        um ponteiro para a sua posição.
        */

        char *nl = strchr(str, '\n');
        if (nl) *nl = '\0';

    }

    printf("Você digitou: %s\n", str);

    return 0;
}
