#include <stdio.h>

// Máximo de caracteres incluindo '\0'...

#define MAX 21

int main() {

    char str[MAX];

    printf("Digite algo (até %d caracteres): ", MAX);

    fgets(str, MAX, stdin);

    printf("Você digitou: %s.", str);

    return 0;
}
