#include <stdio.h>

int main() {

    int numero;
    float medida;
    char ch;
    char nome[30];

    printf("Digite um número: ");
    scanf("%d", &numero);
    printf("Você digitou %d.\n\n", numero);

    printf("Digite um comprimento: ");
    scanf("%f", &medida);
    printf("Você digitou %.4f.\n\n", medida);

    /*
    Neste ponto, as quebras de linha farão com que
    um caractere invisível ainda esteja presente
    em stdin, causando problemas na próxima leitura.

    Isso afeta basicamente o especificador %c, mas
    pode ser resolvido colocando um espaço antes dele:

    scanf(" %c", &ch);

    Ou, nós podemos limpar o buffer de entrada
    com a função 'getchar()', como abaixo...
    */

    while ((getchar()) != '\n');

    printf("Digite um caractere: ");
    scanf("%c", &ch);
    printf("Você digitou %c (%d).\n\n", ch, ch);

    printf("Digite um nome: ");
    scanf("%s", nome);
    printf("Você digitou %s.\n\n", nome);

    return 0;
}
