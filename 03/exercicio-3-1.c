// Aula 3 - Exercício 1

#include <stdio.h>
#include <math.h>

int main() {

    double limite;

    limite = pow(2, (8 * sizeof(int)) - 1);

    printf("Tamanho em bytes: %ld\n", sizeof(int));
    printf("Inteiro máximo: %.f\n", limite - 1);
    printf("Inteiro mínimo: -%.f\n", limite);


    return 0;
}

