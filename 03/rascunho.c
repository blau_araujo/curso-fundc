#include <stdio.h>

int main() {

    char c;

    for (c = 'A'; c <= 'Z'; ++c) {
        printf("Caractere %c - Inteiro %d\n", c, c);
    }

    return 0;
}
