// Aula 3 - Exercício 2
#include <stdio.h>

int main() {

    char c;

    printf("%02x\n", 128);
    for (c = 64; c >= -1 ; c/=2) {
        printf("%02x\n", c);
    }
    printf("%02x\n", 0);

    return 0;
}

