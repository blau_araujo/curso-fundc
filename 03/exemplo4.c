// Aula 3 - Exemplo 4

#include <limits.h>
#include <stdio.h>

int main() {

    printf("Tamanho em bytes: %d\n", sizeof(int));
    printf("Inteiro máximo: %d\n", INT_MAX);
    printf("Inteiro mínimo: %d\n", INT_MIN);

    return 0;
}

