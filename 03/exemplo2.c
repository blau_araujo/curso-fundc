// Aula 3 - Exemplo 2

#include <stdio.h>

int main() {

    char c = 'A';

    printf("Caractere: %c\n", c);
    printf("Caractere (decimal): %d\n", c);
    printf("Caractere (octal)  : %o\n", c);
    printf("Caractere (hexa)   : %x\n", c);

    return 0;
}

