#include <stdio.h>

int main() {

    float f  = 5.0 / 3.0;
    double d = 5.0 / 3.0;

    printf(
           "Float : 5 / 3 = %.20f\n"
           "                  .....|\n"
           "Double: 5 / 3 = %.20lf\n"
           "                  ..............|\n",
           f,
           d
           );

    return 0;
}
