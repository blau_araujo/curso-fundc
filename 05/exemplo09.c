#include <stdio.h>
#include <limits.h>
#include <float.h>

int main() {

    printf(
    "\nunsigned char: %ld byte\n"
    "signed char  : %ld byte\n"
    "SCHAR_MIN    : %d\n"
    "SCHAR_MAX    : %d\n"
    "UCHAR_MAX    : %d\n",
    sizeof(unsigned char),
    sizeof(signed char),
    SCHAR_MIN,
    SCHAR_MAX,
    UCHAR_MAX
    );

    printf(
    "\nunsigned int      : %ld bytes\n"
    "signed int        : %ld bytes\n"
    "short int         : %ld bytes\n"
    "unsigned short int: %ld bytes\n"
    "long int          : %ld bytes\n"
    "unsigned long int : %ld bytes\n"
    "INT_MIN           : %d\n"
    "INT_MAX           : %d\n"
    "UINT_MAX          : %u\n"
    "SHRT_MIN          : %d\n"
    "SHRT_MAX          : %d\n"
    "USHRT_MAX         : %u\n"
    "LONG_MIN          : %ld\n"
    "LONG_MAX          : %ld\n"
    "ULONG_MAX         : %lu\n",
    sizeof(unsigned int),
    sizeof(signed int),
    sizeof(short int),
    sizeof(unsigned short int),
    sizeof(long int),
    sizeof(unsigned long int),
    INT_MIN,
    INT_MAX,
    UINT_MAX,
    SHRT_MIN,
    SHRT_MAX,
    USHRT_MAX,
    LONG_MIN,
    LONG_MAX,
    ULONG_MAX
    );

    printf(
    "\nlong double: %ld bytes\n"
    "LDBL_MIN   : %LE\n"
    "LDBL_MAX   : %LE\n",
    sizeof(long double),
    LDBL_MIN,
    LDBL_MAX
    );

    return 0;
}

