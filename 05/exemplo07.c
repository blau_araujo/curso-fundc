#include <stdio.h>
#include <float.h>

int main() {

    printf(
           "\nTIPO FLOAT\n"
           "----------------\n"
           "Tamanho (bytes): %ld \n"
           "Mínimo         : %E \n"
           "Máximo         : %E \n"
           "Precisão       : %d \n"

           "\nTIPO DOUBLE\n"
           "----------------\n"
           "Tamanho (bytes): %ld \n"
           "Mínimo         : %E \n"
           "Máximo         : %E \n"
           "Precisão       : %d \n\n",
           sizeof(float),
           FLT_MIN,
           FLT_MAX,
           FLT_DIG,
           sizeof(double),
           DBL_MIN,
           DBL_MAX,
           DBL_DIG
           );

    return 0;
}

