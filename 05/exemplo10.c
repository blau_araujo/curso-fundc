#include <stdio.h>
#include <float.h>

int main() {

    long double ld = 5.0 / 3.l;

    printf(
           "Long Double: 5 / 3 = %.20LF\n"
           "                       .................|%d\n",
           ld,
           LDBL_DIG
           );

    return 0;
}

