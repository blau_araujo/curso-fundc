#include <stdio.h>

/*

int *ponteiro;

*ponteiro = &a;

*/

int main() {

    int a = 10;
    int *ponteiro = &a;

    printf("Valor de 'a': %d\n"
           "Endereço de '&a': %p\n"
           "Conteúdo de 'p': %p\n"
           "Valor apontado por '*p': %d\n",
           a, &a, ponteiro, *ponteiro);

    return 0;
}
