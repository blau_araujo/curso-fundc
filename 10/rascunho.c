#include <stdio.h>

int main() {

    int a = 10;
    int *p = &a;

    printf("Valor de a: %d\n"
           "Endereço de a: %p\n"
           "Conteúdo de p: %p\n"
           "Valor apontado por p: %d\n"
           , a, &a, p, *p);

    return 0;
}
