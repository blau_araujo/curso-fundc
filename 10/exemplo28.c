#include <stdio.h>

int main() {

    int a = 10, b = 20;
    int *p = &a;

    puts("*p = &a");
    printf("*p=%d, a=%d, b=%d\n", *p, a, b);

    b = *p;

    puts("b = *p");
    printf("*p=%d, a=%d, b=%d\n", *p, a, b);

    *p = 0;

    puts("*p = 0");
    printf("*p=%d, a=%d, b=%d\n", *p, a, b);

    return 0;
}
